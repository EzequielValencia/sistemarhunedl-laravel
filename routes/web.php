<?php
use PeopleUnedl\Http\Middleware\UsaDispositivoMobile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * Rutas para login y salir de sesion
 */
Route::group( [ 'prefix' => '/' ],function(){
    Route::get('','LoginController@index');
    Route::post('login','LoginController@login');
    Route::get('logout','LoginController@loginOut');
});
/***
 * Rutas de los puestos y rectorias
 */
Route::prefix('/puestosrectorias')->group(function(){
    //Metodos para agregar vicerrectorías
    Route::get('/rectorias','VicerrectoriaController@index');
    Route::post('/rectorias','VicerrectoriaController@store');
    Route::get('/rectorias/damerectoria','VicerrectoriaController@show');
    Route::put('/rectorias','VicerrectoriaController@update');
    Route::delete('/rectorias','VicerrectoriaController@destroy');
    //Metodos para agregar puestos
    Route::get('/puestos','PuestoController@index');
    Route::post('/puestos','PuestoController@store');
    Route::put('/puestos','PuestoController@edit');
    Route::delete('/puestos','PuestoController@destroy');
    Route::get('/puestos/damepuesto','PuestoController@show');
    Route::get('/puestos/puestosporrectoria','PuestoController@puestosPorRectorias');
    //Metodos para planteles
    Route::prefix('/planteles')->group(function(){
        Route::get('','PlantelController@index');
        Route::post('/guardar','PlantelController@store');
        Route::put('/guardar','PlantelController@edit');
        Route::get('/plantel/{idPlantel?}','PlantelController@show');
    });
});


/**
 * Rutas relacionadas con los usuarios
 */
Route::prefix('/usuarios')->group(function(){
    Route::get('','UsuarioController@index');
    Route::post('','UsuarioController@store');
    Route::delete('','UsuarioController@destroy');
    Route::put('','UsuarioController@edit');
    Route::get('/usuario','UsuarioController@show');
    Route::post('/cambiarpassword','UsuarioController@cambiarPassword');
    Route::get('/historial/{id}','UsuarioController@historialUsuario');
});
/***
 * Rutas de los empleados
 */
Route::prefix('/empleados')->group(function(){
    Route::get('/todos','EmpleadoController@todos');
    Route::get('/vicerrectoria/{rectoria}','EmpleadoController@index');
    Route::get('/dadosdebaja','EmpleadoController@dadosDeBaja');
    Route::get('/agregarempleado','EmpleadoController@create');
    Route::post('/guardar','EmpleadoController@store');
    Route::get('/ver','EmpleadoController@show');
    Route::get('/empleado','EmpleadoController@getEmpleado');
    Route::post('/editar','EmpleadoController@edit');
    Route::put('/dardebaja','EmpleadoController@darDeBaja');
    Route::put('/restaurarempleado','EmpleadoController@restaurarEmpleado');
    Route::delete('/eliminar','EmpleadoController@destroy');
    Route::get('/buscarporrfccurpimss','EmpleadoController@buscarPorCurpRfcOIMSS');
    Route::get('/jefesporpuestosubordinado','EmpleadoController@getJefesDeLosPuestos');
    Route::post('/guardatrayectoria','EstudioController@store');
    Route::put('/guardatrayectoria','EstudioController@editarEstudios');
    Route::get('/estudios/{idEmpleado}','EstudioController@estudiosEmpleado');
    Route::get('/estudios/estudio/{id}','EstudioController@estudio');
    Route::put('/estudios/editar','EstudioController@editEstudio');
    Route::delete('/estudios/eliminar/{id}','EstudioController@eliminarEstudio');
});
/**
 * Rutas para la oferta academica
 */
Route::prefix('/ofertaacademica')->group(function(){
   Route::get('','GradoEstudioController@index');
   Route::post('','GradoEstudioController@store');
   Route::put('','GradoEstudioController@update');
   Route::delete('','GradoEstudioController@destroy');
   Route::get('/gradoEstudio','GradoEstudioController@show');
});

Route::prefix('/carrerascursos')->group(function (){
    Route::get('','CarreraController@index');
    Route::post('','CarreraController@store');
    Route::put('','CarreraController@update');
    Route::delete('','CarreraController@destroy');
    Route::get('/carrera','CarreraController@show');
});

Route::prefix('/materias')->group(function (){
    Route::get('','MateriaController@index');
    Route::post('','MateriaController@store');
    Route::put('','MateriaController@update');
    Route::delete('','MateriaController@destroy');
    Route::get('/materia','MateriaController@show');
    Route::get('/curso/{idCurso}','MateriaController@materiasPorCurso');
});

Route::prefix('/horario')->group(function(){
   Route::post('/crearHorario','HorarioController@store');
   Route::get('/{codigoEmpleado}','HorarioController@index');
   Route::get('/empleado/{idEmpleado}','HorarioController@cargaHorariaPorEmpleado');
});


Route::prefix('/reportes')->group(function(){
    Route::get('','ReportesController@index');
    Route::get('/reporteEnPdf/{fechaInicio}/{fechaFin}/{vicerrectoria}/{puesto}','ReportesController@reporteEnPdf');
    /*Route::get('/generar/{formato?}','ReportesController@creaReportePersonalUnedl');*/
});


