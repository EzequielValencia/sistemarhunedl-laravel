<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedInteger('id_grado_estudio');
            $table->foreign('id_grado_estudio')->references('id')->on('grado_estudios')->onDelete('cascade');
            $table->unsignedInteger('creado_por');
            $table->foreign('creado_por')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('modificado_por')->nullable();
            $table->foreign('modificado_por')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreras');
    }
}
