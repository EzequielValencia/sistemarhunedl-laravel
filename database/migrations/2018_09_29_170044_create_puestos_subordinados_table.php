<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuestosSubordinadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puestos_subordinados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_puesto_padre');
            $table->foreign('id_puesto_padre')->references('id')->on('puestos')->onDelete('cascade');
            $table->unsignedInteger('id_puesto_hijo');
            $table->foreign('id_puesto_hijo')->references('id')->on('puestos')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puestos_subordinados');
    }
}
