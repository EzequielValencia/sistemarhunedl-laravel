<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccesos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accesos', function (Blueprint $table) {
            //
            $table->string('ip');
            $table->boolean('es_escritorio');
            $table->boolean('es_tablet');
            $table->boolean('es_celular');
            $table->boolean('es_android');
            $table->boolean('es_iphone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accesos', function (Blueprint $table) {
            //
        });
    }
}
