<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            //Datos personales
            $table->string('nombre');
            $table->string('apellido_paterno');
            $table->string('apellido_materno')->nullable();
            $table->date('fecha_nacimiento');

            $table->string('curp');
            $table->string('rfc');
            $table->string('numero_imss');
            $table->string('numero_infonavit');

            $table->string('lugar_de_nacimiento');
            $table->string('cedula')->nullable();
            $table->string('titulo')->nullable();
            $table->string('estatus_cedula')->nullable();
            $table->string('cp');
            $table->string('domcicilio');
            $table->date('fecha_matrimonio')->nullable();
            $table->string('telefono_de_emergencias');
            $table->string('telefono');
            $table->string('celular');
            $table->string('correo');

            //Datos de trabajo

            $table->string('codigo');
            $table->date('fecha_ingreso');
            $table->date('fecha_alta_imss');
            $table->string('salario');
            $table->string('imagen')->nullable();
            $table->string('estado_civil');
            $table->unsignedInteger('jefe')->nullable();
            $table->foreign('jefe')->references('id')->on('empleados')->onDelete('cascade');

            //Datos para el sistema. De esta manera saben quien lo creo y lo modifico
            $table->unsignedInteger('creado_por');
            $table->foreign('creado_por')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('modificado_por')->nullable();
            $table->foreign('modificado_por')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
