<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpelados20181125 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empleados',function (Blueprint $empleados){
           $empleados->timestamp('fecha_baja')->nullable();
           $empleados->boolean('activo')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleados',function (Blueprint $empleados){
            $empleados->dropColumn('fecha_baja');
            $empleados->dropColumn('activo');
        });
    }
}
