<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpleados20181209 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empleados', function (Blueprint $table) {
            //
            $table->string('sexo',2)->nullable();
            $table->string('patron');
            $table->unsignedInteger('plantel_id');
            $table->foreign('plantel_id')->references('id')->on('plantels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleados', function (Blueprint $table) {
            //
            $table->dropColumn('sexo');
            $table->dropColumn('patron');
            $table->dropColumn('plantel_id');
        });
    }
}
