<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class GradoEstudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('grado_estudios')
            ->insert([[
                'nombre'=>'Educación Media Superior',
                'codigo'=>'',
                'created_at'=>now()
                ],
                [
                    'nombre'=>'Licenciatura',
                    'codigo'=>'',
                    'created_at'=>now()
                ],
                [
                    'nombre'=>'Posgrado',
                    'codigo'=>'',
                    'created_at'=>now()
                ],
                [
                    'nombre'=>'Educación Continua',
                    'codigo'=>'',
                    'created_at'=>now()
                ]
        ]);
    }
}
