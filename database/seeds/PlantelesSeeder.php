<?php

use Illuminate\Database\Seeder;

class PlantelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('plantels')->insert([
            ['nombre'=>'Ciencias de la Salud',
                'direccion'=>'Av. Enrique Díaz de León sur No. 404',
                'telefono'=>'3825 3871 - 3825 3521',
                'email'=>'ciencias@unedl.edu.mx'],

            ['nombre'=>'Bachillerato General por Competencias',
                'direccion'=>'Av. Hidalgo No.1393 Col. Ladrón de Guevara',
                'telefono'=>'3825 1251-3825 3171',
                'email'=>'bachillerato@unedl.edu.mx'],

            ['nombre'=>'Económico Administrativas y Artes',
                'direccion'=>'Av. Enrique Díaz de León No. 324',
                'telefono'=>'3827-3001, 3827- 3002, 3827- 3638',
                'email'=>'económico@unedl.edu.mx'],

            ['nombre'=>'Ciencias de la Salud',
                'direccion'=>'Av. Enrique Díaz de León sur No. 404',
                'telefono'=>'3825 3871 - 3825 3521 ',
                'email'=>'ciencias@unedl.edu.mx'],

            ['nombre'=>'Ciencias Sociales e Ingeniería',
                'direccion'=>'Av. Enrique Díaz de León No. 90',
                'telefono'=>'3827 0904, 3827 09 05',
                'email'=>'ciencias@unedl.edu.mx'],

            ['nombre'=>'Gastronomía, Turismo y Bachillerato Intensivo Semiescolarizado (BIS)',
                'direccion'=>'Av. Vallarta No. 1151',
                'telefono'=>'3827 3184 y 3825 7182',
                'email'=>'gastronomía@unedl.edu.mx'],

            ['nombre'=>'Posgrados y Educación continua',
                'direccion'=>'Av. Enrique Díaz de León No. 90',
                'telefono'=>'18152265',
                'email'=>'posgrados@unedl.edu.mx'],

            ['nombre'=>'Servicios a la Comunidad',
                'direccion'=>'Contreras Medellín No.16, Col.Centro',
                'telefono'=>'36145214 y 36145412',
                'email'=>'servicios@unedl.edu.mx']
        ]);
    }
}
