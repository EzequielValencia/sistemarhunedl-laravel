<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class VicerrectoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('vicerrectorias')->insert([[
            'codigo'=>'0001',
            'nombre'=>'Vicerrectoría Academica',
            'descripcion'=>'',
            'creado_por'=>1,
            'created_at'=>now()
        ],[
            'codigo'=>'0002',
            'nombre'=>'Vicerrectoría Administrativa',
            'descripcion'=>'',
            'creado_por'=>1,
            'created_at'=>now()
        ],[
            'codigo'=>'0003',
            'nombre'=>'Vicerrectoría Ejecutiva',
            'descripcion'=>'',
            'creado_por'=>1,
            'created_at'=>now()
        ],[
            'codigo'=>'0004',
            'nombre'=>'Rectoria',
            'descripcion'=>'',
            'creado_por'=>1,
            'created_at'=>now()
        ]]);
    }
}
