<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use PeopleUnedl\Vicerrectoria;

class PuestosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vicerrectoriaAcademica = Vicerrectoria::where('codigo','0001')->first();
        $vicerrectoriaAdministrativa = Vicerrectoria::where('codigo','0002')->first();
        $vicerrectoriaEjecutiva = Vicerrectoria::where('codigo','0003')->first();
        $rectoria = Vicerrectoria::where('codigo','0004')->first();
        DB::table('puestos')->insert([
            /*
             ----Comenzamos con la inserción de los puestos principales de vicerectoria academica
             */
            [
                'nombre'=>'Vicerrector Academico',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Dirección de Posgrados y Educación Continua',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Dirección Académica',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Dirección de Vinculación y Extensión',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Dirección Académica de Ciencias de la Salud',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de la Licenciatura en Psicología',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de las Licenciaturas en Administración de Empresas y Contaduría Pública',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de la Licenciatura en Mercadotecnia y Negocios Internacionales',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de la Licenciatura en Derecho',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de la Licenciatura en Derecho Semiescolarizado',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            [
                'nombre'=>'Coordinación de la Licenciatura en Turismo',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de la Licenciatura en Gastronomía',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de la Licenciatura en Diseño para la Comunicación Gráfica',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de la Licenciatura en Ingeniería de Software',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de la Licenciatura en Arquitectura',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de la Licenciatura en Ciencias de la Comunicación',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Bachillerato por Competencias',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Prácticas Profesionales y Servicio Social',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Egresados',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Arte, Cultura y Deportes',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Clínica de UNEDL TV',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Responsable de Clínica de UNEDL Radio',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Responsable del Centro Privado de Prestación de Servicio de Métodos Alternativos',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Centro de Atención Integral Psicológica (CAIP)',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Responsable del Centro de Atención Nutricional (CAN)',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Responsable del Centro de Atención Jurídica',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Centro de Emprendimiento e Incubación (PRENDE)',
                'codigo'=>'',
                'rectoria_id'=> $vicerrectoriaAcademica->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            /*
             --- Vicerrectoria Administrativa
             */
            [
                'nombre'=>'Vicerrector Administrativo',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Dirección de Formación y Desarrollo de Talento',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Infraestructura Tecnológica y Telecomunicaciones (ITT)',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Contabilidad y Finanzas',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Control Escolar Económicas Administrativas y Artes, Gastronomía y Turismo',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Control Escolar Bachillerato Intensivo Semiescolarizado',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Control Escolar Licenciaturas UdeG y Bachillerato General por Competencias',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinadora de Capital Humano',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Control Escolar Posgrados, Derecho e Ingeniería de Software',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaAdministrativa->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            /*
             --- Vicerrectoria Ejecutiva
             */
            [
                'nombre'=>'Vicerrector Ejecutivo',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaEjecutiva->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Relaciones Públicas y Protocolo',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaEjecutiva->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Comunicación Institucional',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaEjecutiva->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Imagen Institucional',
                'codigo'=>'',
                'rectoria_id'=>$vicerrectoriaEjecutiva->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
            /*
             --- Rectoria
             */
            [
                'nombre'=>'Rectoría',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Dirección de Planeación, Innovación y Gestión Académica',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Dirección de Calidad',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Dirección Legal',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Tutoría y Orientación Educativa',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Calidad',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Bibliotecas UNEDL',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Evaluación',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],[
                'nombre'=>'Coordinación de Diseño Curricular',
                'codigo'=>'',
                'rectoria_id'=>$rectoria->id,
                'creado_por'=>1,
                'vacantes'=>1,
                'vacantes_usadas'=>1
            ],
        ]);
    }
}
