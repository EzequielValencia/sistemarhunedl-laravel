<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'user_name'=>'ezequiel',
            'password'=>bcrypt('1234'),
            'nombre'=>'Ezequiel',
            'paterno'=>'Valencia',
            'materno'=>'Moreno',
            'role'=>'admin',
            'activo'=>true,
            'created_at'=>now()
        ],[
            'user_name'=>'admin',
            'password'=>bcrypt('administrador'),
            'nombre'=>'Admin',
            'paterno'=>'Admin',
            'materno'=>'Admin',
            'role'=>'admin',
            'activo'=>true,
            'created_at'=>now()
        ],[
            'user_name'=>'user',
            'password'=>bcrypt('usuario'),
            'nombre'=>'Usuario',
            'paterno'=>'Usuario',
            'materno'=>'Usuario',
            'role'=>'user',
            'activo'=>true,
            'created_at'=>now()
        ]]);
    }
}
