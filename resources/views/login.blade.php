@extends('mainlayout')

@section('css')
    <link href="css/login.css" rel="stylesheet">
@stop

@section('content')
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">
                <img src="imagenes/indice.png" style="width: 100%;">
            </div>
            <div class="card-body">
                <form method="POST" action="{{url('/login')}}">
                    <p class="text-muted text-center">Ingresa tu usuario y contraseña</p>
                    <div class="form-group">
                        <div class="form-label-group">
                            {{csrf_field()}}
                            <input type="text" id="inputEmail" class="form-control" placeholder="Usuario" name="usuario" required="required" >
                            <label for="inputEmail">USUARIO</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="required" name="password">
                            <label for="inputPassword">Password</label>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

    @if(isset($error) && isset($mensaje))
        <script>

            window.onload = function(){swal('Uups!!','{{$mensaje}}','error');}

        </script>
    @elseif(isset($error) && !isset($mensaje))
        <script>
            window.onload = function(){swal('Uups!!','Error al iniciar sesión. Intentalo otra vez','error');}
        </script>
    @endif
@stop
