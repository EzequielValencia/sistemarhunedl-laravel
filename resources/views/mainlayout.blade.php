<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema de control del personal UNEDL">
    <meta name="author" content="Ezequiel Valencia Moreno">
    <link rel="shortcut icon" href="{{asset('imagenes/logo1.png')}}" type="image/x-icon">
    <title>PEOPLEUNEDL</title>

    <!-- Bootstrap core CSS-->
    <link href="adminpanel/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="adminpanel/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="adminpanel/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="adminpanel/css/sb-admin.css" rel="stylesheet">
    <link href="{{asset('bower_components/sweetalert2/dist/sweetalert2.css')}}">
    <link href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css">
    @yield('css')
</head>

<body id="page-top">



    @yield('content')
<!-- Bootstrap core JavaScript-->
<script src="adminpanel/vendor/jquery/jquery.min.js"></script>
<script src="adminpanel/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="adminpanel/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="adminpanel/vendor/chart.js/Chart.min.js"></script>
<script src="adminpanel/vendor/datatables/jquery.dataTables.js"></script>
<script src="adminpanel/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="adminpanel/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="adminpanel/js/demo/datatables-demo.js"></script>
<script src="adminpanel/js/demo/chart-area-demo.js"></script>
<script src="{{asset('bower_components/sweetalert2/dist/sweetalert2.all.js')}}"></script>
@yield('js')
</body>

</html>
