@extends('principalLayout')
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Vicerrectorías
            <button class="btn btn-sm btn-success pull-right" title="Agregar una vicerrectoría"
                    data-toggle="modal" data-target="#agregarRectoria">
                <i class="fas fa-plus"></i> Agregar
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($vicerrectorias as $vicerrectoria)
                        <tr>
                            <td></td>
                            <td>{{$vicerrectoria->codigo}}</td>
                            <td>{{$vicerrectoria->nombre}}</td>
                            <td>{{$vicerrectoria->descripcion}}</td>
                            <td>
                                <div class="container">
                                    <button class="btn btn-sm btn-warning" title="Agregar una vicerrectoría" data-idvicerrectoria="{{$vicerrectoria->id}}"
                                            data-toggle="modal" data-target="#editarRectoria">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger" title="Agregar una vicerrectoría" data-idvicerrectoria="{{$vicerrectoria->id}}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>


    <div class="modal fade" id="agregarRectoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Vicerrectoría</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div >
                        <div class="form-group">
                            <div class="form-label-group">
                                {{csrf_field()}}
                                <input type="text" id="codigo" class="form-control" placeholder="Codigo" name="codigo" required="required" >
                                <label for="codigo">Codigo</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">

                                <input type="text" id="nombre" class="form-control" placeholder="Usuario" name="nombre" required="required" >
                                <label for="inputEmail">Nombre</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <textarea id="descripcion" class="form-control"  required="required" name="descripcion" placeholder="Descripción"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarNuevaRectoria">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editarRectoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Vicerrectoría</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div >
                        <div class="form-group">
                            <div class="form-label-group">
                                {{csrf_field()}}
                                <input type="hidden" id="idEditar">
                                <input type="text" id="codigoEditar" class="form-control" placeholder="Codigo" name="codigo" required="required" >
                                <label for="codigo">Codigo</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">

                                <input type="text" id="nombreEditar" class="form-control" placeholder="Usuario" name="nombre" required="required" >
                                <label for="inputEmail">Nombre</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <textarea id="descripcionEditar" class="form-control"  required="required" name="descripcion" placeholder="Descripción"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarEdicion">Editar</button>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')
    <script src="{{asset('js/vicerectorias/vicerectorias.js')}}"></script>
@endsection