@extends('principalLayout')
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Grados de estudio
            <button class="btn btn-sm btn-success pull-right" title="Agregar grado de estudio"
                    data-toggle="modal" data-target="#agregarGradoEstudio">
                <i class="fas fa-plus"></i> Agregar
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($gradosDeEstidio as $gradoEstudio)
                            <tr>
                                <td></td>
                                <td>{{$gradoEstudio->codigo}}</td>
                                <td>{{$gradoEstudio->nombre}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col">
                                            <button class="btn btn-sm btn-warning editarGradoEstudio" data-grado_id="{{$gradoEstudio->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn-sm btn-danger eliminarGradoEstudio" data-grado_id="{{$gradoEstudio->id}}">
                                                <i class="fa fa-eraser"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="agregarGradoEstudio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Grado de Estudio</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="formNuevoGradoEstudio">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" id="idGradoEstudio">
                                <label for="codigo">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" id="nombre" required>
                            </div>
                            <div class="col-md-6">
                                <label for="nombre">Código</label>
                                <input type="text" class="form-control" placeholder="Código" id="codigo" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardar">Guardar</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script src="{{asset('js/ofertaAcademica/ofertaAcademica.js')}}"></script>
@endsection