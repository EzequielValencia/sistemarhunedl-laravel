@extends('principalLayout')
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-chart-pie"></i> Generador de reportes

            <button type="button" class="m-1 btn btn-success float-right"  title="Generar reporte" id="generarReporte">
                <i class="fas fa-book-open"></i> Generar Reporte
            </button>
            <div class="dropdown float-right m-1">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Elige el formato
                </button>
                <div class="dropdown-menu">
                    <div class="dropdown-item" >
                        <input type="checkbox" aria-label="Checkbox for following text input" id="activos">
                        <i class="fas fa-file-pdf"></i> PDF
                    </div>
                    <div class="dropdown-item" >
                        <input type="checkbox" aria-label="Checkbox for following text input" id="activos">
                        <i class="fas fa-file-word"></i> Word
                    </div>
                    <div class="dropdown-item" >
                        <input type="checkbox" aria-label="Checkbox for following text input" id="activos">
                        <i class="fas fa-file-excel"></i> Excel
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <label for="">Fecha inicio</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar-alt"></i>
                            </span>
                        </div>
                        <input type="date" id="fecha-inicio" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Fecha fin</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar-alt"></i>
                            </span>
                        </div>
                        <input type="date" id="fecha-fin" class="form-control"  aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="input-group mb-3 col-md-6">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Vicerrectoria</label>
                            </div>
                            <select class="custom-select" id="vicerectoria" required>
                                <option selected value="-1">Escoge una vicerrectoría</option>
                                <option value="todas">Todas</option>
                                @foreach($vicerrectorias as $vicerrectoria)
                                    <option value="{{$vicerrectoria->id}}">{{$vicerrectoria->nombre}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-group mb-3 col-md-6">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Puestos</label>
                            </div>
                            <select class="custom-select" id="puestos" disabled="disabled" required>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer small text-muted">

        </div>
    </div>
@endsection
@section('js')
    <script >
        let urlPrincipal = '{{url('')}}';
        $(document).ready( function() {
            $("#fecha-inicio").val(primerDiaDelMes());
            $("#fecha-fin").val(fechaActual());
        });

        $("#fecha-inicio").change(function(){
           validadorDeFechas($(this).val(),$("#fecha-fin").val());
        });

        $("#fecha-fin").change(function () {
            validadorDeFechas($("#fecha-inicio").val(),$(this).val());
        });

        $("#vicerectoria").change(function(){
            let idRectoria = $(this).val();
            if(idRectoria!=0 && idRectoria != 'todas') {
                $.ajax({
                    url: urlPrincipal + '/puestosrectorias/puestos/puestosporrectoria',
                    method: 'GET',
                    data: {
                        rectoriaId: idRectoria
                    }, success: function (data) {
                        $("#puestos").empty();
                        let opciones = `<option value="-1"> Selecciona un puesto </option> <option value="todos">Todos</option> `;
                        data.forEach(function (puesto, index) {
                            opciones += `<option value="${puesto.id}"> ${puesto.nombre} </option>`;
                        });
                        $("#puestos").append(opciones);
                        $("#puestos").prop('disabled', false);
                    }, error: function (error) {
                        console.log(error);
                    }
                })
            }else{
                $("#puestos").empty();
                $("#puestos").prop('disabled', true);
            }
        })

        $("#labelDadosDeBaja").click(function () {
            $("#dadosDeBaja").prop( "checked", !$("#dadosDeBaja").is(':checked'));
        });

        $("#labelActivos").click(function () {
           $("#activos").prop( "checked", !$("#activos").is(':checked') );
        });

        $("#generarReporte").click(function () {
            let fechaInicio = $("#fecha-inicio").val().replace(/\//g, "-");
            let fechaFin = $("#fecha-fin").val().replace(/\//g, "-");
            let vicerrectoria = $("#vicerectoria").val();
            let puesto = $("#puestos").val();
            if(vicerrectoria == -1){
                swal('warning','Debes seleccionar una vicerrectoria')
                return;
            }
            if(puesto == -1){
                swal('warning','Debes seleccionar un puesto o el campo todos para poser generar el reporte');
            }
            window.location.href = `/reportes/reporteEnPdf/${fechaInicio}/${fechaFin}/${vicerrectoria}/${puesto}`;
        })
    </script>
@endsection