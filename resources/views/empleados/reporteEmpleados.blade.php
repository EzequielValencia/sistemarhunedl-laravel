<!doctype html>
<html lang="es">
<head>
    <style>
        *{
            font-family: sans-serif;
        }
        .titulo-principal{
            width: 100%;
            padding: 10px;
            font-size: 18px;
            color:#fff;
            background-color: #213F80;
            text-align: center;
            text-transform: capitalize;
        }
        .titulo-principal img{
            width: 180px; height: 40px;
            float: left;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
            text-align: center;
        }
        th{
            font-size: 14px;
        }
        table{
            margin-top: 30px;
        }

        td{
            width: 20%;
        }
        caption{
            font-weight: bolder;
            font-size: 18px;
            border: 1px solid black;
        }
    </style>
</head>
<body>

    <h1 class="titulo-principal">
        Indice de rotación de personal
    </h1>
    <p>
        Periodo {{strftime('%d %B %G',strtotime ($fechaInicio)).' - '.strftime('%d %B %G',strtotime ($fechaFin))}}
    </p>

    @foreach($relacionPuestosCantidades as $vicerrectoria)
        <div>
            <h4>{{$vicerrectoria['vicerrectoria']}}</h4>
            @foreach($vicerrectoria['puestos'] as $puesto)
                <table>
                    <caption>Puesto: {{$puesto->nombre}}</caption>
                    <tr>
                        <th>Personal contratado durante el periodo</th>
                        <th>Personal dado de baja durante el periodo</th>
                        <th>Numero de empleados existentes al inicio del periodo</th>
                        <th>Numero de empleados existentes al final del periodo</th>
                        <th>Indice de rotación</th>
                    </tr>
                    <tbody>
                        <td>{{$puesto->cantidad_empleados_contratados_durante_el_periodo}}</td>
                        <td>{{$puesto->cantidad_empleados_dados_de_baja_durante_el_periodo}}</td>
                        <td>{{$puesto->cantidad_empleados_existentes_al_inicio_del_periodo}}</td>
                        <td>{{$puesto->cantidad_empleados_existentes_al_final_del_periodo}}</td>
                        <td>{{$puesto->indice_de_rotacion}}</td>
                    </tbody>
                </table>            
            @endforeach
        </div>
    @endforeach

</body>
</html>