@extends('principalLayout')
@section('css')
    <style>
        .hidden{
            display: none;
        }
    </style>
@stop
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-calendar"></i> Carga horaria de {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}
            <div class="pull-right">
                <a href="{{url('/empleados/ver').'?id='.$empleado->id}}" class="btn  btn-primary" data-toggle="tooltip" title="Regresar">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
                <button type="button" class="btn btn-success" id="guardarCargaHoraria" >
                    <i class="fa fa-save"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-md-4">
                    <label class="control-label">Carrera Curso</label>
                    <select class="validate form-control " id="carreraCurso">
                        <option value="">Selecciona un valor</option>
                        @foreach ($carreras as $carrera)
                            <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-md-4">
                    <label class="control-label">Materia</label>
                    <select class="validate form-control " id="materia">
                    </select>
                </div>
                <div class="col-xs-12 col-md-2">
                    <label class="control-label">Horas</label>
                    <input type="number" class="validate form-control cargaHoraria"  placeholder="Carga Horaria" id="cargaHoraria" min="0"/>
                </div>
                <div class="col-xs-12 col-md-2 agregarQuitarCargaHoraria" >
                    <label class="control-label">&nbsp;</label>
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-primary" id="agregarCargaHoraria" disabled="disabled" >
                            <i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-info" id="reiniciarSelectores">
                            <i class="fas fa-redo-alt"></i>
                        </button>
                        <button type="button" class="btn btn-danger" id="borrarHorario" >
                            <i class="fa fa-eraser"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="table-responsive col-xs-12 p-3">
                    <table class="table table-bordered table-sm table-hover" id="tablaCargaHoraria">
                        <thead class="text-center">
                            <tr >
                                <th>Curso o Carrera</th>
                                <th>Materia</th>
                                <th>Carga Horaria</th>
                                <th>Quitar carga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalHoras = 0
                            @endphp
                            @foreach ($empleado->cargaHoraria()->get() as $index => $cargaHoraria)
                                <tr>
                                    <td class="p-2">{{$cargaHoraria->materia->carreraCurso->nombre}}</td>
                                    <td class="p-2">{{$cargaHoraria->materia->nombre}}</td>
                                    <td class="p-2">
                                        <input type="number" class="validate form-control form-control-sm cargaHoraria"
                                               placeholder="Carga Horaria" id="cargaHoraria" value="{{$cargaHoraria->horas}}"
                                                onchange="cambiarCargaHoraria({{$index}},this)" min="1"/>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-danger btn-sm" onclick=" quitarCargaHoraria({{$index}}) ">
                                            <i class="fa fa-eraser"></i>
                                        </button>
                                    </td>
                                </tr>
                                @php
                                    $totalHoras +=$cargaHoraria->horas
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="card-footer">

            <strong id="totalHoras">Horas Totales: {{$totalHoras}}</strong>

        </div>
    </div>
@stop
@section('js')
    <script >
        let urlPrincipal = "{{url('/')}}";
        let idEmpleado = {{$empleado->id}};
    </script>
    <script src="{{asset('js/cargaHoraria/cargaHoraria.js')}}"></script>
@endsection