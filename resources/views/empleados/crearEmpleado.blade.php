@extends('principalLayout')
@section('css')
    <style>
        .hidden{
            display: none;
        }
    </style>
@stop
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Nuevo Empleado
        </div>
        <div class="card-body">
            <form id="crearEmpleado" action="{{url('/empleados/guardar')}}" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    {{csrf_field()}}
                    <div class="form-group col-md-12">
                        <h3 class="control-label">Datos Personales</h3>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-md-12" style="padding: 0% 25%;">
                            <img src="{{asset('storage/e5E07LZMVmf69mUs1BZN4wovEGsyKldL8pnozbeX.png')}}" height="150px" id="verImagen" width="100%"/>
                        </div>
                        <label class="btn btn-file bt-lg btn-block btn-primary">
                            Selecciona imagen
                            <input type="file" id="imagenProfesor" accept="image/*" style="display: none;" name="imagen" />
                        </label>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-md-12 ">
                            <label class="control-label">Codigo</label>
                            <input type="text" class="validate form-control" required="required" name="codigo" id="codigo" />
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Fecha Ingreso</label>
                            <input type="date" class="validate form-control" required="required" name="fecha_ingreso" id="fecha_ingreso" />
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Fecha Alta IMSS</label>
                            <input type="date" class="validate form-control" required="required" name="fecha_alta_imss" id="fecha_alta_imss" />
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Genero</label>
                            <select name="sexo" id="" class="form-control" required>
                                <option value="h">Hombre</option>
                                <option value="m">Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="control-label">Nombre</label>
                        <input type="text" class="validate form-control" required="required" name="nombre" id="nombre" />
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label ">Apellido Paterno</label>
                        <input type="text" class="validate form-control" required="required" name="apellido_paterno" id="apellidoPaterno" />
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Apellido Materno</label>
                        <input type="text" class="validate form-control"  name="apellido_materno" id="apellidoMaterno" />
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Fecha Nacimiento</label>
                        <input type="date" id="fechaNacimiento" name="fecha_nacimiento" required="required"  class="form-control" />
                    </div>

                </div>
                <div class="form-row">

                    <div class="col-md-3">
                        <label class="control-label">CURP</label>
                        <label class="error-label text-danger hidden small"><small>CURP invalido</small></label>
                        <input class="validate[required] form-control" required="required" type="text" name="curp" id="curp" maxlength="20"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">RFC</label>
                        <label class="error-label text-danger hidden small"><small>RFC invalido</small></label>
                        <input class="validate[required] form-control" required="required" type="text" name="rfc" id="rfc" maxlength="13"/>

                    </div>

                    <div class="col-md-3">
                        <label class="control-label">No. IMSS</label>
                        <label class="error-label text-danger hidden small"><small>Numero de IMSS invalido</small></label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="numero_imss" id="numero_imss"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">No. INFONAVIT</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="numero_infonavit" id="infonavit" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4">
                        <label class="control-label">Lugar de nacimiento</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="lugar_de_nacimiento" id="lugarNacimiento"/>
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Domicilio actual</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="domcicilio" id="domicilio" />
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Codigo Postal</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="cp" id="cp" maxlength="5"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">
                        <label class="control-label">Telefono local</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="telefono" id="telefono" maxlength="12"/>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Celular</label>
                        <input class="validate[required,custom[email]] form-control" type="text"  name="celular" id="celular" maxlength="12"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Telefono emergencia</label>
                        <input class="validate[required,custom[email]] form-control"  type="text" required="required" name="telefono_de_emergencias" id="telefono_de_emergencias"	 maxlength="12"/>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Correo</label>
                        <input class="validate[required,custom[email]] form-control"  type="email" required="required" name="correo" id="correo" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label class="control-label">Estado civil</label>
                        <select name="estado_civil" id="estado_civil" class="validate form-control" required="required">
                            <option value="">Estado civil..</option>
                            <option value="casado">Casado</option>
                            <option value="soltero">Soltero</option>
                            <option value="Viudo">Viudo</option>
                            <option value="divorciado">Divorciado</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Fecha de matrimonio</label>
                        <input type="date" id="fecha_matrimonio" name="fecha_matrimonio" disabled="disabled" class="form-control" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3 class="control-label">Cargo y puesto</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12" >
                        <label class="control-label">Cargo</label>
                        <select name="cargo[]" id="cargo" class="validate form-control" multiple="multiple" >
                            @foreach($puestosPorRectorias as $puestosPorRectoria)
                                <optgroup label="{{$puestosPorRectoria['vicerrectoria']}}">
                                    @foreach($puestosPorRectoria['puestos'] as $puesto)
                                        <option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Salario</label>
                        <input type="number" class="validate form-control" name="salario" id="salario" placeholder="$0.00" required="required"/>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Jefe Directo</label>
                        <select name="jefe" id="jefe" class="validate form-control" disabled="disabled">

                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Patron</label>
                        <select name="patron" id="patron" class="validate form-control" required>
                            <option >Selecciona el patron...</option>
                            <option value="CUNEDL">CUNEDL</option>
                            <option value="IVEDL">IVEDL</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label class="control-label">Plantel en el que labora</label>
                        <select name="plantel" id="plantel" class="validate form-control" required>
                            <option value="">Selecciona el plantel...</option>
                            @foreach($planteles as $plantel)
                                <option value="{{$plantel->id}}">{{$plantel->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="col-12">
                    <input type="submit" value="Guardar" class="btn btn-primary pull-left"/>
                    <input type="reset" value="Reiniciar" class="btn btn-danger pull-right"/>
                </div>
            </form>
        </div>
        <div class="card-footer">

        </div>
    </div>
@stop
@section('js')
    <script >
        $(document).ready( function() {
            $("#fecha_ingreso").val(fechaActual());
            $("#fecha_alta_imss").val(fechaActual());

        });
    </script>
    <script src="{{asset('js/empleados/empleados.js')}}"></script>
    <script src="{{asset('js/util/validadores.js')}}"></script>
@endsection