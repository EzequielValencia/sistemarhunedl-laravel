@extends('principalLayout')
@section('css')
    <style>
        #contextMenu {
            position: absolute;
            display: none;
        }
    </style>
@endsection
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            @if(isset($titulo))
                <i class="fas fa-university"></i> Empleados {{$titulo}}
            @else
                <i class="fas fa-university"></i> Empleados
            @endif
            <a class="btn btn-sm btn-success float-right" title="Agregar una vicerrectoría" href="{{url('empleados/agregarempleado')}}">
                <i class="fas fa-plus"></i> Agregar Empleado
            </a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-sm" id="tabla" width="100%" cellspacing="0">
                    <thead >
                    <tr>
                        <th></th>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cargos</th>
                        <th>Salario</th>
                        <th>Celular</th>
                        <th>Tel Emergencia</th>
                        <th>Teléfono</th>
                        <th>Dirección</th>
                        <th>CP</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($empleados as $empleado)
                        <tr data-empleadoId="{{$empleado->id}}" style="cursor: pointer;">
                            <td data-empleadoId="{{$empleado->id}}"></td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->codigo}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}</td>
                            <td data-empleadoId="{{$empleado->id}}">
                                @if($empleado->puestosEmpleado()->count()>0)
                                    <ul>
                                        @foreach($empleado->misPuestos() as $puesto)
                                            <li>{{$puesto->nombre}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <strong>Sin cargos</strong>
                                @endif
                            </td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->salario}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->celular}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->telefono_de_emergencias}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->telefono}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->domcicilio}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->cp}}</td>
                            <td data-empleadoId="{{$empleado->id}}">{{$empleado->correo}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
    <div id="contextMenu" class="card">
        <ul class="list-group list-group-flush">
            <li class="list-group-item text-justify">
                <a class="text-dark" id="verInfoEmpleado" style="text-decoration: none;" href="#">Ver información <i class="far fa-eye pull-right"></i> </a>
            </li>
            <li class="list-group-item text-justify">
                <a class="text-dark" id="eliminarEmpleado" style="text-decoration: none;" href="#">
                    Eliminar <i class="fas fa-trash-alt pull-right"></i>
                </a>
            </li>
        </ul>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/util/menuContextual.js')}}"></script>
    @if (session('guardado'))
        <script>
            window.onload = function(){swal('Muy bien','Se ha guardado la información correctamente','success');}
        </script>
    @elseif(session('editado'))
        <script>
            window.onload = function(){swal('Muy bien','Se ha editado la información correctamente','success');}
        </script>
    @endif
    <script >
        let urlVerEmpleados = ""+'{{url('empleados/ver')}}';
        $("table tbody tr").click(function(){
            let idEmpleado = $(this).data('empleadoid');
            window.location.href = urlVerEmpleados+'?id='+idEmpleado;
        });
    </script>
@endsection