@extends('principalLayout')
@section('css')
    <style>
        .hidden{
            display: none;
        }
    </style>
@stop
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Imformación de {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}
            <div class="pull-right">
                @if($empleado->activo != true)
                    <button class="btn btn-sm btn-danger" data-toggle="tooltip"
                            title="Eliminar a {{$empleado->nombre.' '.$empleado->apelldio_paterno.' '.$empleado->apellido_materno}}"
                            id="eliminarEmpleado"
                            data-id="{{$empleado->id}}">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-sm btn-success"
                            title="Restaurar {{$empleado->nombre.' '.$empleado->apelldio_paterno.' '.$empleado->apellido_materno}}"
                            id="restaurarEmpleado" data-idEmpleado="{{$empleado->id}}">
                        <i class="fas fa-user-check"></i>
                    </button>
                @else
                    <a href='{{url('/horario/'.$empleado->codigo)}}' class="btn btn-sm btn-dark" data-toggle="tooltip" title="Carga horaria">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </a>
                    <button class="btn btn-sm btn-danger" data-toggle="tooltip"
                            title="Eliminar a {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}"
                            id="eliminarEmpleado"
                            data-id="{{$empleado->id}}">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-sm btn-info"
                            title="Agregar estudios a {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}"
                            id="agregarEstudios"
                            data-id="{{$empleado->id}}">
                        <i class="fas fa-graduation-cap"></i>
                    </button>
                    <button class="btn btn-sm btn-warning"
                            title="Dar de baja a {{$empleado->nombre.' '.$empleado->apellido_paterno.' '.$empleado->apellido_materno}}"
                            id="darDeBajaEmpleado">
                        <i class="fas fa-user-times"></i>
                    </button>
                @endif
            </div>
        </div>
        <div class="card-body">
            <form action="{{url('/empleados/editar')}}" method="POST" enctype="multipart/form-data">
                <div class="form-row">
                    {{csrf_field()}}
                    <input type="hidden" name="idEmpleado" id="idEmpleado" value="{{$empleado->id}}">
                    <div class="form-group col-md-12">
                        <h3 class="control-label">Datos Personales</h3>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-md-12" style="padding: 0% 25%;">
                            <img src="{{asset(Storage::url($empleado->imagen))}}" height="170px" id="verImagen" width="100%"/>
                        </div>
                        <label class="btn btn-file bt-lg btn-block btn-primary">
                            Selecciona imagen
                            <input type="file" id="imagenProfesor" accept="image/*" style="display: none;" name="imagen" />
                        </label>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-md-12 ">
                            <label class="control-label">Codigo</label>
                            <input type="text" class="validate form-control" required="required" name="codigo" id="codigo" value="{{$empleado->codigo}}"/>
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Fecha Ingreso</label>
                            <input type="date" class="validate form-control" required="required" name="fecha_ingreso" id="fecha_ingreso" value="{{$empleado->fecha_ingreso}}"/>
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Fecha Alta IMSS</label>
                            <input type="date" class="validate form-control" required="required" name="fecha_alta_imss" id="fecha_alta_imss" value="{{$empleado->fecha_alta_imss}}"/>
                        </div>
                        <div class="col-md-12 ">
                            <label class="control-label">Genero</label>
                            <select name="sexo" id="" class="form-control" required>
                                <option value="h" @if($empleado->sexo=='h') selected @endif>Hombre</option>
                                <option value="m" @if($empleado->sexo=='m') selected @endif>Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label class="control-label">Nombre</label>
                        <input type="text" class="validate form-control" required="required" name="nombre" id="nombre" value="{{$empleado->nombre}}"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label ">Apellido Paterno</label>
                        <input type="text" class="validate form-control" required="required" name="apellido_paterno" id="apellidoPaterno" value="{{$empleado->apellido_paterno}}"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Apellido Materno</label>
                        <input type="text" class="validate form-control"  name="apellido_materno" id="apellidoMaterno" value="{{$empleado->apellido_materno}}"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label">Fecha Nacimiento</label>
                        <input type="date" id="fechaNacimiento" name="fecha_nacimiento" required="required"  class="form-control" value="{{$empleado->fecha_nacimiento}}"/>
                    </div>

                </div>
                <div class="form-row">

                    <div class="col-md-3">
                        <label class="control-label">CURP</label>
                        <label class="error-label text-danger hidden small"><small>CURP invalido</small></label>
                        <input class="validate[required] form-control" required="required" type="text" name="curp" id="curp" maxlength="20" value="{{$empleado->curp}}"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">RFC</label>
                        <label class="error-label text-danger hidden small"><small>RFC invalido</small></label>
                        <input class="validate[required] form-control" required="required" type="text" name="rfc" id="rfc" maxlength="13" value="{{$empleado->rfc}}"/>

                    </div>

                    <div class="col-md-3">
                        <label class="control-label">No. IMSS</label>
                        <label class="error-label text-danger hidden small"><small>Numero de IMSS invalido</small></label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="numero_imss" id="numero_imss" value="{{$empleado->numero_imss}}"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">No. INFONAVIT</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="numero_infonavit" id="infonavit" value="{{$empleado->numero_infonavit}}"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4">
                        <label class="control-label">Lugar de nacimiento</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="lugar_de_nacimiento" id="lugarNacimiento" value="{{$empleado->lugar_de_nacimiento}}"/>
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Domicilio actual</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="domcicilio" id="domicilio" value="{{$empleado->domcicilio}}"/>
                    </div>

                    <div class="col-md-4">
                        <label class="control-label">Codigo Postal</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="cp" id="cp" maxlength="5" value="{{$empleado->cp}}"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">
                        <label class="control-label">Telefono local</label>
                        <input class="validate[required,custom[email]] form-control" type="text" required="required" name="telefono" id="telefono" maxlength="12" value="{{$empleado->telefono}}"/>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Celular</label>
                        <input class="validate[required,custom[email]] form-control" type="text"  name="celular" id="celular" maxlength="12" value="{{$empleado->celular}}"/>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label">Telefono emergencia</label>
                        <input class="validate[required,custom[email]] form-control"  type="text" required="required" name="telefono_de_emergencias" id="telefono_de_emergencias" maxlength="12" value="{{$empleado->telefono_de_emergencias}}"/>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Correo</label>
                        <input class="validate[required,custom[email]] form-control"  type="email" required="required" name="correo" id="correo" value="{{$empleado->correo}}"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <label class="control-label">Estado civil</label>
                        <select name="estado_civil" id="estado_civil" class="validate form-control" required="required" >
                            <option value="">Estado civil..</option>
                            <option value="casado" {{$empleado->estado_civil == 'casado'?'selected':''}}>Casado</option>
                            <option value="soltero" {{$empleado->estado_civil == 'soltero'?'selected':''}}>Soltero</option>
                            <option value="Viudo" {{$empleado->estado_civil == 'Viudo'?'selected':''}}>Viudo</option>
                            <option value="divorciado" {{$empleado->estado_civil == 'divorciado'?'selected':''}}>Divorciado</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Fecha de matrimonio</label>
                        <input type="date" id="fecha_matrimonio" name="fecha_matrimonio" disabled="disabled" class="form-control" value="{{$empleado->fecha_matrimonio}}"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3 class="control-label">Cargo y puesto</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12" >
                        <label class="control-label">Cargo</label>
                        <select name="cargo[]" id="cargo" class="validate form-control" multiple="multiple" value="1">
                            @foreach($puestosPorRectorias as $puestosPorRectoria)
                                <optgroup label="{{$puestosPorRectoria['vicerrectoria']}}">
                                    @foreach($puestosPorRectoria['puestos'] as $puesto)
                                        <option value="{{$puesto->id}}" {{$empleado->tengoAsignadoEstePuesto($puesto->id)? 'selected':''}} >{{$puesto->nombre}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Salario</label>
                        <input type="number" class="validate form-control" name="salario" id="salario" placeholder="$0.00" required="required" value="{{$empleado->salario}}"/>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Jefe Directo</label>
                        <select name="jefe" id="jefe" class="validate form-control" disabled="disabled">

                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Patron</label>
                        <select name="patron" id="patron" class="validate form-control" required>
                            <option >Selecciona el patron...</option>
                            <option value="CUNEDL" @if($empleado->patron =='CUNEDL') selected @endif>CUNEDL</option>
                            <option value="IVEDL" @if($empleado->patron =='IVEDL') selected @endif>IVEDL</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label class="control-label">Plantel en el que labora</label>
                        <select name="plantel" id="plantel" class="validate form-control" required>
                            <option value="">Selecciona el plantel...</option>
                            @foreach($planteles as $plantel)
                                @if($plantel->id == $empleado->plantel_id)
                                    <option value="{{$plantel->id}}"  selected>{{$plantel->nombre}}</option>
                                @else
                                    <option value="{{$plantel->id}}" >{{$plantel->nombre}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="col-12">
                    <input type="submit" value="Guardar" class="btn btn-primary pull-right"/>
                    <input type="reset" value="Reiniciar" class="btn btn-danger pull-left"/>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-12">
                    <h3>Trayectoria Académica</h3>
                    <div class="table-responsive">
                        <table class="table text-center table-sm table-bordered" >
                            <thead>
                            <tr>
                                <th>Institución</th>
                                <th>Grado</th>
                                <th>Titulo</th>
                                <th>Cedula</th>
                                <th>Situación</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody class="text-left">
                                @forelse($empleado->estudios()->get() as $estudio)
                                    <tr>
                                        <td>{{$estudio->institucion}}</td>
                                        <td>{{$estudio->grado_de_estudio}}</td>
                                        <td>{{$estudio->titulo}}</td>
                                        <td>{{$estudio->cedula}}</td>
                                        <td>{{$estudio->situacion_cedula}}</td>
                                        <td>
                                            <button class="btn btn-sm btn-warning editEstudio" data-idestudio="{{$estudio->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-sm btn-danger eliminarEstudio" data-idestudio="{{$estudio->id}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="6">
                                            No se ha agregado ningun estudio
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">

        </div>
    </div>


    {{--Modal para agregar los grados de esutdio del empleado--}}
    <div class="modal fade " tabindex="-1" role="dialog" id="modalAgregarEstudios">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Grados de esutidio</h5>
                    <button type="button" class="close" data-dismiss="modal" arial-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-row" id="formTrayectoriaAcademica">
                        <div class="col-md-12">
                            <label class="control-label">Institución</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="institucion" id="institucion" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Grado de estudio</label>
                            <select name="grado_de_estudio" id="grado_de_estudio" class="validate form-control" required="required" >
                                <option value="">Selecciona el grado de estudio</option>
                                <option value="Primaria" >Primaria</option>
                                <option value="Secundaria" >Secundaria</option>
                                <option value="Preparatoria" >Preparatoria</option>
                                <option value="Licenciatura" >Licenciatura</option>
                                <option value="Maestria" >Maestria</option>
                                <option value="Doctorado" >Doctorado</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">T&iacute;tulo</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="titulo" id="titulo" disabled="disabled"/>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Cedula Profesional</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="cedula" id="cedula" disabled="disabled" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Situaci&oacute;n</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="estatus_cedula" id="situacionActualCedula" disabled="disabled"/>
                        </div>
                        <div class="col-md-12 mt-1">
                            <button type="submit" class="btn btn-sm btn-success pull-right">Agregar A Trayectoria</button>
                            <button type="reset" class="btn btn-sm btn-default pull-right mr-2">Reiniciar</button>
                        </div>
                    </form>

                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <h5 >Trayectoria Académica</h5>
                            <table class="table text-center table-sm table-bordered" id="tablaTrayectoriaAcademica">
                                <thead>
                                <tr>
                                    <th>Institución</th>
                                    <th>Grado</th>
                                    <th>Titulo</th>
                                    <th>Cedula</th>
                                    <th>Situación</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="text-left">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-success" id="guardarTrayectoria" >Guardar</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal para editar un estudio del emeplado--}}
    <div class="modal fade " tabindex="-1" role="dialog" id="modalEditarEstudio">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Estudio</h5>
                    <button type="button" class="close" data-dismiss="modal" arial-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-row" id="formTrayectoriaAcademicaEditar">
                        <div class="col-md-12">
                            <input type="hidden" id="idEstudioEdit" name="id">
                            <label class="control-label">Institución</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="institucion" id="institucionEdit" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Grado de estudio</label>
                            <select name="grado_de_estudio" id="grado_de_estudio_edit" class="validate form-control" required="required" >
                                <option value="">Selecciona el grado de estudio</option>
                                <option value="Primaria" >Primaria</option>
                                <option value="Secundaria" >Secundaria</option>
                                <option value="Preparatoria" >Preparatoria</option>
                                <option value="Licenciatura" >Licenciatura</option>
                                <option value="Maestria" >Maestria</option>
                                <option value="Doctorado" >Doctorado</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">T&iacute;tulo</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="titulo" id="tituloEdit" disabled="disabled"/>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Cedula Profesional</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="cedula" id="cedulaEdit" disabled="disabled" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Situaci&oacute;n</label>
                            <input class="validate[required,custom[email]] form-control" type="text" name="situacion_cedula" id="situacionActualCedulaEdit" disabled="disabled"/>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-success" id="guardarTrayectoriaEdit" >Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('js/empleados/empleados.js')}}"></script>
    <script src="{{asset('js/empleados/trayectoriaAcademica.js')}}"></script>
    <script src="{{asset('js/util/validadores.js')}}"></script>
    @if($empleado->activo!=true)
        <script >
            $('input').prop('disabled',true);
            $('select').prop('disabled',true);
            $("#eliminarEmpleado").prop('disabled',false);
            $("#restaurarEmpleado").prop('disabled',false);
        </script>
    @endif
@endsection