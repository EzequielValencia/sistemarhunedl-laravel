@extends('principalLayout')
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-history"></i> Historial del usuario {{"$usuario->nombre $usuario->paterno $usuario->materno"}}
            <a class="btn btn-sm btn-success pull-right" title="Agregar usuario" href='{{url("/usuarios")}}'>
                <i class="fas fa-arrow-left"></i> Regresar
            </a>
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#historial" role="tab" aria-controls="home" aria-selected="true">Actividad</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#access" role="tab" aria-controls="home" aria-selected="true">Accesos</a>
                </li>
            </ul>
        </div>

        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="historial" role="tabpanel" aria-labelledby="home-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Mensaje</th>
                                <th>Hora de incidente</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuario->dameHistorial()->get() as $registro)
                                <tr>
                                    <td></td>
                                    <td>{{$registro->mensaje}}</td>
                                    <td>{{$registro->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="access" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabla2" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Fecha y hora</th>
                                <th>Dispositivo</th>
                                <th>S.O</th>
                                <th>Navegador</th>
                                <th>Autorizado</th>
                                <th>IP</th>
                                <th>Escritorio</th>
                                <th>Tablet</th>
                                <th>Celular</th>
                                <th>Android</th>
                                <th>Iphone</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuario->dameAccessos()->get() as $aceso)
                                <tr class="{{!$aceso->permitido? 'bg-danger':''}}">
                                    <td></td>
                                    <td>{{$aceso->hora_acceso}}</td>
                                    <td>{{$aceso->dispositivo}}</td>
                                    <td>{{$aceso->sistema_operativo}}</td>
                                    <td>{{$aceso->navegador}}</td>
                                    <td>{{$aceso->permitido ? 'Sí':'No'}}</td>
                                    <td>{{$aceso->ip}}</td>
                                    <td>{{$aceso->es_escritorio ? 'Sí':'No'}}</td>
                                    <td>{{$aceso->es_tablet ? 'Sí':'No'}}</td>
                                    <td>{{$aceso->es_celular ? 'Sí':'No'}}</td>
                                    <td>{{$aceso->es_android ? 'Sí':'No'}}</td>
                                    <td>{{$aceso->es_iphone ? 'Sí':'No'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{--<div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Mensaje</th>
                        <th>Hora de incidente</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuario->dameHistorial()->get() as $registro)
                        <tr>
                            <td></td>
                            <td>{{$registro->mensaje}}</td>
                            <td>{{$registro->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>--}}
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
@stop
@section('js')
    <script >
        $('#tabla2').DataTable();
    </script>
@endsection