@extends('principalLayout')
@section('css')
    <style>
        #dropdown-herrmientas .dropdown-item{
            color: #4b4b4b;
        }
        #dropdown-herrmientas .dropdown-item:hover{
            color: #b3b9bf;
        }
    </style>
@endsection
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Usuarios
            <div class="dropdown pull-right">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Herramientas
                </button>
                <div id="dropdown-herrmientas" class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="color: #4b4b4b">
                    <a class="dropdown-item" href="#">
                        <i class="fas fa-database"></i> Respaldos
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Cambiar tiempo de inactividad</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#guardarUsuario">
                        <i class="fas fa-user"></i> Agregar usuario
                    </a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>usuario</th>
                        <th>Estatus</th>
                        <th>Ultimo acceso</th>
                        <th>Accedio desde</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($usuarios as $usuario)
                            @php
                            $ultimoAccesso = $usuario->dameAccessos()->orderBy('created_at','desc')->first();
                            @endphp
                            <tr>
                                <td></td>
                                <td>{{"$usuario->nombre $usuario->paterno $usuario->materno"}}</td>
                                <td>{{$usuario->user_name}}</td>
                                <td>{{$usuario->activo ? 'activo': 'inactivo'}}</td>
                                <td>{{!is_null($ultimoAccesso)?$ultimoAccesso->hora_acceso:''}}</td>
                                <td>{{!is_null($ultimoAccesso)?"$ultimoAccesso->sistema_operativo $ultimoAccesso->navegador":''}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col">
                                            <button data-user_id="{{$usuario->id}}" class="btn btn-sm btn-warning editar" title='editar a {{"$usuario->nombre $usuario->paterno $usuario->materno"}}'
                                                    {{Auth::user()->id == $usuario->id? 'disabled':''}} >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <button data-user_id="{{$usuario->id}}" class="btn btn-sm {{$usuario->activo? 'btn-success':'btn-danger'}} activarDesactivarUsuario" title="{{$usuario->activo ? 'desactivar usuario':'activar usuario'}}"
                                                    {{Auth::user()->id == $usuario->id? 'disabled':''}}>
                                                <i class="fa fa-power-off"></i>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn-sm btn-primary cambiarContrasena" data-user_id="{{$usuario->id}}" title="Cambiar contraseña"
                                                    {{Auth::user()->id == $usuario->id? 'disabled':''}}>
                                                <i class="fa fa-key"></i>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn-sm btn-danger eliminarUsuario" data-user_id="{{$usuario->id}}"
                                                    {{Auth::user()->id == $usuario->id? 'disabled':''}}>
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <a class="btn btn-sm btn-dark" href='{{url("usuarios/historial/$usuario->id")}}'>
                                                <i class="fas fa-history"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="guardarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="formNuevoUsuario">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="hidden" id="idUsuario">
                                <label for="codigo">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" id="nombre" required>
                            </div>
                            <div class="col-md-4">
                                <label for="nombre">Apellido Paterno</label>
                                <input type="text" class="form-control" placeholder="Apellido Paterno" id="paterno" required>
                            </div>
                            <div class="col-md-4">
                                <label for="nombre">Apellido Materno</label>
                                <input type="text" class="form-control" placeholder="Apellido Materno" id="materno">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="nombre">Nombre de Usuario</label>
                                <input type="text" class="form-control" placeholder="Nombre de usuario" id="user">
                            </div>
                            <div class="col-md-6">
                                <label for="vicerrectoria">Rol</label>
                                <select class="form-control" id="role" name="role" required="required">
                                    <option selected disabled>Selecciona una rol de usuario</option>
                                    <option value="admin">Administrador</option>
                                    <option value="user">Usuario</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="nombre">Contraseña</label>
                                <input type="password" class="form-control" placeholder="contraseña" id="password" required>
                            </div>
                            <div class="col-md-6">
                                <label for="nombre">Confirma Contraseña</label>
                                <input type="password" class="form-control" placeholder="confirma contraseña" id="confirmPassword" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarNuevoUsuario">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="cambiarContraseña" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cambiar contraseña</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="formCambiarPassword">
                        <div class="row">
                            <input type="hidden" id="usuarioParaCambiarPassword">
                            <div class="col-md-6">
                                <label for="nombre">Contraseña nueva</label>
                                <input type="password" class="form-control" placeholder="contraseña" id="passwordNuevo" required>
                            </div>
                            <div class="col-md-6">
                                <label for="nombre">Confirma Contraseña</label>
                                <input type="password" class="form-control" placeholder="confirma contraseña" id="confirmPasswordNuevo" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarNuevoPassword">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/usuarios/usuarios.js')}}"></script>
@endsection