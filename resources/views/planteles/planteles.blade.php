@extends('principalLayout')
@section('css')
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Planteles
            <button class="btn btn-sm btn-success pull-right" title="Agregar un plantel"
                    data-toggle="modal" data-target="#agregarPlantel">
                <i class="fas fa-plus"></i> Agregar
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-sm" id="tabla" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Dirección</th>
                        <th>Contacto</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($planteles as $plantel)
                        <tr>
                            <td></td>
                            <td>{{$plantel->nombre}}</td>
                            <td>{{$plantel->direccion}}</td>
                            <td>Tel:{{$plantel->telefono}}<br>Email:{{$plantel->email}}</td>
                            <td>
                                <div class="container">
                                    <button class="btn btn-sm btn-warning" title="Editar Plantel" data-plantelid="{{$plantel->id}}"
                                            data-toggle="modal" data-target="#editarPuesto">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger" title="Eliminar Plantel" data-plantelid="{{$plantel->id}}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>

    <div class="modal fade" id="agregarPlantel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Plantel</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div >
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" id="idPlantel">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre del plantel" id="nombre">
                            </div>
                            <div class="col-sm-12">

                                <label for="direccion">Dirección</label>
                                <input type="text" class="form-control" placeholder="Dirección del plantel" id="direccion">
                            </div>
                            <div class="col-sm-6">

                                <label for="email">Email</label>
                                <input type="text" class="form-control" placeholder="email" id="email">
                            </div>
                            <div class="col-sm-6">
                                <label for="telefono">Telefono</label>
                                <input type="text" class="form-control" placeholder="Dirección del plantel" id="telefono">
                            </div>
                            <div class="col-sm-12">
                                <label for="telefono">Descripcion</label>
                                <textarea class="form-control"  id="descripcion" cols="5" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarPlantel">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('js/planteles/planteles.js')}}"></script>
@endsection
