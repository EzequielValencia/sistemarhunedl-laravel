@extends('principalLayout')
@section('css')
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('cuerpo')
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-university"></i> Puestos
            <button class="btn btn-sm btn-success pull-right" title="Agregar una vicerrectoría"
                    data-toggle="modal" data-target="#agregarPuesto">
                <i class="fas fa-plus"></i> Agregar
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Vacantes</th>
                        <th>Vacantes Libres</th>
                        <th>Rectoria</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($puestos as $puesto)
                        <tr>
                            <td></td>
                            <td>{{$puesto->codigo}}</td>
                            <td>{{$puesto->nombre}}</td>
                            <td>{{$puesto->vacantes}}</td>
                            <td>{{$puesto->vacantes - $puesto->vacantes_usadas}}</td>
                            <td>{{$puesto->vicerrectoria->nombre}}</td>
                            <td>
                                <div class="container">
                                    <button class="btn btn-sm btn-warning" title="Editar vicerrectoría" data-puestoid="{{$puesto->id}}"
                                            data-toggle="modal" data-target="#editarPuesto">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger" title="Eliminar vicerrectoría" data-puestoid="{{$puesto->id}}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>

    <div class="modal fade" id="agregarPuesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Puesto</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="row">
                            <div class="col">
                                <label for="codigo">Codigo</label>
                                <input type="text" class="form-control" placeholder="Codigo" id="codigo">
                            </div>
                            <div class="col">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" id="nombre">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="nombre">Vacantes</label>
                                <input type="number" class="form-control" placeholder="No de vacantes" id="vacantes" min="0" step="1">
                            </div>
                            <div class="col">
                                <label for="vicerrectoria">Vicerrectoria</label>
                                <select class="form-control" id="vicerrectoria" name="vicerrectoria" required="required" style="width: 100%">
                                    <option value="0">Selecciona una vicerrectoría</option>
                                    @foreach($vicerrectorias as $vicerrectoria)
                                        <option value="{{$vicerrectoria->id}}">{{$vicerrectoria->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="vicerrectoria">Subordinados</label>
                                <select class="form-control" id="subordinados" name="vicerrectoria" required="required" style="width: 100%" multiple="multiple">
                                    <option value="0">Selecciona una vicerrectoría para llenar este campo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarNuevoPuesto">Guardar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editarPuesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Puesto</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="row">
                            <div class="col">
                                <label for="codigo">Codigo</label>
                                <input type="hidden" id="idEditar">
                                <input type="text" class="form-control" placeholder="Codigo" id="codigoEditar">
                            </div>
                            <div class="col">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" id="nombreEditar">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="vicerrectoria">Vicerrectoria</label>
                                <select class="form-control" id="vicerrectoriaEditar" name="vicerrectoria" required="required" style="width: 100%">
                                    <option value="0">Selecciona una vicerrectoría</option>
                                    @foreach($vicerrectorias as $vicerrectoria)
                                        <option value="{{$vicerrectoria->id}}">{{$vicerrectoria->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label for="nombre">Vacantes</label>
                                <input type="number" class="form-control" placeholder="No de vacantes" id="vacantesEditar" min="0" step="1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="vicerrectoria">Subordinados</label>
                                <select class="form-control" id="subordinadosEditar" name="vicerrectoria" required="required" style="width: 100%" multiple="multiple">
                                    <option value="0">Selecciona una vicerrectoría para llenar este campo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="guardarPuestoEditado">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('js/puestos/puestos.js')}}"></script>
@endsection
