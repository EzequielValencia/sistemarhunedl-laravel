<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema de control del personal UNEDL">
    <meta name="author" content="Ezequiel Valencia Moreno">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('imagenes/logo1.png')}}" type="image/x-icon">
    <title>PEOPLEUNEDL</title>

    <!-- Bootstrap core CSS-->
    <link href="{{asset('adminpanel/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="{{asset('adminpanel/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="{{asset('adminpanel/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <link href="{{asset('adminpanel/css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('css/navbar.css')}}" rel="stylesheet">
    <link href="{{asset('bower_components/sweetalert2/dist/sweetalert2.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css">

    @yield('css')
</head>

<body id="page-top">


<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="{{url('/')}}">
        <img src="{{asset('imagenes/logo.png')}}" style="width: 200px; height: 50px;"/>
    </a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </div>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="#">Activity Log</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>
        </li>
    </ul>

</nav>

<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav toggled">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="empleados" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-briefcase"></i>
                <span>Empleados</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="empleados">
                <a class="dropdown-item" href="{{url('/empleados/todos')}}">Todos</a>
                @foreach($vicerrectorias as $vicerrectoria)
                    <a class="dropdown-item" href="{{url('/empleados/vicerrectoria/'.$vicerrectoria->codigo)}}">{{$vicerrectoria->nombre}}</a>
                @endforeach
                <a class="dropdown-item" href="{{url('/empleados/dadosdebaja')}}">Bajas</a>
                <a class="dropdown-item" href="{{url('/reportes')}}">Generar Reporte</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="rectoriasYPuestos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-graduation-cap"></i>
                <span>Vicerrectorías, Puestos y Planteles</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="rectoriasYPuestos">
                <a class="dropdown-item"  href="{{url('puestosrectorias/rectorias')}}">
                    <i class="fa fa-university" aria-hidden="true"></i>&nbsp;Vicerrectorías
                </a>
                <a class="dropdown-item"  href="{{url('puestosrectorias/puestos')}}">
                    <i class="fa fa-building" aria-hidden="true"></i>&nbsp; Puestos
                </a>

                <a class="dropdown-item"  href="{{url('puestosrectorias/planteles')}}">
                    <i class="fa fa-building" aria-hidden="true"></i>&nbsp; Planteles
                </a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="ofertaAcademica" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-school"></i>
                <span>Oferta academica</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="ofertaAcademica">
                <a class="dropdown-item" href="{{url('/ofertaacademica')}}">Oferta Academica</a>
                <a class="dropdown-item" href="{{url('/carrerascursos')}}">Carreras y Cursos</a>
                <a class="dropdown-item" href="{{url('/materias')}}">Materias</a>
            </div>
        </li>
        @if (Auth::user()->role=='admin')
            <li class="nav-item">
                <a class="nav-link" href="{{url('usuarios')}}" id="ofertaAcademica">
                    <i class="fas fa-fw fa-user"></i>
                    <span>Usuarios</span>
                </a>
            </li>
        @endif
    </ul>

    <div id="content-wrapper">
        <div class="container-fluid">
            @yield('cuerpo')
        </div>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Estás seguro de querer cerrar la sesión?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Oprime "Salir" para abondonar el sitio. De lo contrario oprime cancelar</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-primary" href="{{url('/logout')}}">Salir</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('adminpanel/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('adminpanel/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('adminpanel/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Page level plugin JavaScript-->


<script src="{{asset('adminpanel/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminpanel/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('adminpanel/js/sb-admin.min.js')}}"></script>

<!-- Demo scripts for this page-->
<script src="{{asset('adminpanel/js/demo/datatables-demo.js')}}"></script>
<script src="{{asset('adminpanel/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('bower_components/sweetalert2/dist/sweetalert2.all.js')}}"></script>
<script src="{{asset('js/util/util.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('js')
</body>

</html>
