$(document).ready(function() {
    $('#vicerrectoria').select2({
        theme:'bootstrap4',
        dropdownParent: $('#agregarPuesto')
    });

});
/**
 * Al seleccionar una vicerrectoria
 * Se llena el campo select con los puestos que pertenecen a la vicerrectoria.
 * Esto para poder llenar el campo de que puestos seran subordinados del puesto que se esta dando de alta
 */
$("#vicerrectoria").change(function(){
 let idRectoria = $(this).val();
 $.ajax({
     url:'puestos/puestosporrectoria',
     type:'GET',
     data:{
         rectoriaId:idRectoria
     }
 }).done(function (data) {
     let opciones='';
     $("#subordinados").empty();
    data.forEach(function(puesto,index){
        opciones+='<option value="'+puesto.id+'">'+puesto.nombre+'</option>';
    });
    $("#subordinados").append(opciones);
     $('#subordinados').select2({
         theme:'bootstrap4',
         dropdownParent: $('#agregarPuesto')
     });
 }).fail(function(){

 });
});

$("#guardarNuevoPuesto").click(function () {
   let nombre=$("#nombre").val();
   let codigo=$("#codigo").val();
   let vicerrectoria =$("#vicerrectoria").val();
   let subordinados = $("#subordinados").val();
   let vacantes = $("#subordinados").val();
   $.ajax({
       url:'puestos',
       type: 'POST',
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       data:{
           nombre:nombre,
           codigo:codigo,
           vicerrectoria:vicerrectoria,
           subordinados:subordinados,
           vacantes: vacantes
       }
   }).done(function(data){
       if(data.status == 200){
            setTimeout(function(){
                location.reload();
            },2500);
           Alertas.exito('Muy bien!',data.mensaje)
       }else{
           Alertas.error("Uuups!",'Ocurrio un error al querer guardar el puesto');
       }
   }).fail(function(){

   });
});

$("#guardarPuestoEditado").click(function(){
    let nombre=$("#nombreEditar").val();
    let codigo=$("#codigoEditar").val();
    let vicerrectoria =$("#vicerrectoriaEditar").val();
    let subordinados = $("#subordinadosEditar").val();
    let vacantes = $("#vacantesEditar").val();
    let id = $("#idEditar").val();
    $.ajax({
        url:'puestos',
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            id:id,
            nombre:nombre,
            codigo:codigo,
            vicerrectoria:vicerrectoria,
            subordinados:subordinados,
            vacantes:vacantes
        }
    }).done(function(data){
        if(data.status == 200){
            setTimeout(function(){
                location.reload();
            },2500);
            Alertas.exito('Muy bien!',data.mensaje)
        }else{
            Alertas.error("Uuups!",'Ocurrio un error al querer guardar el puesto');
        }
    }).fail(function(){

    });
});
/**
 * Metodo para pintar los datos del puesto que deseamos editar.
 */
$(".btn-warning").click(function(){
   let idPuesto = $(this).data('puestoid');
   $.ajax({
       url:'puestos/damepuesto',
       type:'GET',
       data:{
           id:idPuesto
       }
   }).done(function(data){
        let puesto = data;
        $("#idEditar").val(puesto.id);
        $("#nombreEditar").val(puesto.nombre);
        $("#codigoEditar").val(puesto.codigo);
        $("#vicerrectoriaEditar").val(puesto.rectoria_id);
        $("#vicerrectoriaEditar").select2({
           theme:'bootstrap4',
           dropdownParent: $('#editarPuesto')
        });
        $("#vacantesEditar").val(puesto.vacantes);
        $.ajax({
            url:'puestos/puestosporrectoria',
            type:'GET',
            data:{
                rectoriaId:puesto.rectoria_id
            }
        }).done(function(data){
            let opciones='';
            let puestosSubordinados=[];
            $("#subordinadosEditar").empty();
            puesto.subordinados.forEach(function(puesto){
                puestosSubordinados.push(puesto.id);
            });
            data.forEach(function(puesto1,index){
                opciones+='<option value="'+puesto1.id+'">'+puesto1.nombre+'</option>';
            });
            $("#subordinadosEditar").append(opciones);
            $("#subordinadosEditar").val(puestosSubordinados);
            $('#subordinadosEditar').select2({
                theme:'bootstrap4',
                dropdownParent: $('#editarPuesto')
            });
        }).fail(function () {

        });

   }).fail(function(error){

   });
});

/**
 * Elimina el puesto seleccionado
 */
$(".btn-danger").click(function(){
   let idPuesto = $(this).data('puestoid');//Id del puesto que se quiere eliminar

   $.ajax({
       url:'puestos/damepuesto',
       type:'GET',
       data:{
           id:idPuesto
       }
   }).done(function(data){//Consegudos los datos se muestra el swal de confirmación
       swal({
           title: 'ADVERTENCIA!!!',
           text: "Estas apunto de eliminar el puesto "+data.nombre+". Si lo eliminas afectaras a los puestos y empleados que esten relacionados.",
           type: 'warning',
           showCancelButton: true,
           customClass: 'animated tada',
           confirmButtonColor: '#d33',
           cancelButtonColor: '#3085d6',
           confirmButtonText: 'Eliminar',
           cancelButtonText: 'Conservar'
       }).then((result) => {
           if (result.value) {
               $.ajax({
                   type:'DELETE',
                   url:'puestos',
                   data:{
                       id:idPuesto
                   }
               }).done(function(data){
                   if(data.status == 200){
                       $("#agregarRectoria").modal('hide');
                       Alertas.exito('Muy bien!',data.mensaje);
                   }else{
                       Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                   }
               }).fail(function(error){
                   Alertas.error('Uups!','Ocurrio un error al guardar el registro');
               });
           }else{
               swal(
                   'Muy bien!',
                   ''+data.nombre+' concervada.',
                   'success'
               )
           }
       });
   }).fail(function(error){
      console.log(error);
   });
});