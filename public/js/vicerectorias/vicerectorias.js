/**
 * Al cerrar el modal para agregar vicerrectoria se devuelven los valores vacios
 */
$("#agregarRectoria").on('hidden.bs.modal',()=>{
    $("#nombre").val('');
    $("#descripcion").val('');
    $("#codigo").val('');
});

/**
 * Guarda la nueva rectoria que se crea en la vista
 */
$("#guardarNuevaRectoria").click(()=>{
   let nombre = $("#nombre").val();
   let descripcion = $("#descripcion").val();
   let codigo = $("#codigo").val();
   let token = $('meta[name="csrf-token"]').attr('content');
   $.ajax({
       type:'POST',
       url:'rectorias',
       data:{
           nombre:nombre,
           descripcion:descripcion,
           codigo:codigo
       }
   }).done(function(data){
        if(data.status == 200){
            $("#agregarRectoria").modal('hide');
            Alertas.exito('Muy bien!',data.mensaje);
        }else{
            Alertas.error('Uups!','Ocurrio un error al guardar el registro');
        }
   }).fail(function(error){
       Alertas.error('Uups!','Ocurrio un error al guardar el registro');
   });
});
/**
 * Trae los datos de la vicerrectoría seleccionada
 */
$(".btn-warning").click(function(){
    let vicerrectoria = $(this).data('idvicerrectoria');
    $.ajax({
        type:'GET',
        url:'rectorias/damerectoria',
        data:{
            id:vicerrectoria
        }
    }).done(function(data){
        $("#idEditar").val(data.id);
        $("#codigoEditar").val(data.codigo);
        $("#nombreEditar").val(data.nombre);
        $("#descripcionEditar").val(data.descripcion);
    }).fail(function(error){
        console.log(error);
    });
});
/***
 * Metodo para actualizar los datos de la vicerrectoria seleccionada
 */
$("#guardarEdicion").click(function(){
    let nombre = $("#nombreEditar").val();
    let descripcion = $("#descripcionEditar").val();
    let codigo = $("#codigoEditar").val();
    let id = $("#idEditar").val();
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type:'PUT',
        url:'rectorias',
        data:{
            id:id,
            nombre:nombre,
            descripcion:descripcion,
            codigo:codigo
        }
    }).done(function(data){
        if(data.status == 200){
            $("#agregarRectoria").modal('hide');
            Alertas.exito('Muy bien!',data.mensaje);
        }else{
            Alertas.error('Uups!','Ocurrio un error al guardar el registro');
        }
    }).fail(function(error){
        Alertas.error('Uups!','Ocurrio un error al guardar el registro');
    });
});

$(".btn-danger").click(function () {
   let id = $(this).data('idvicerrectoria');
    $.ajax({
        type:'GET',
        url:'rectorias/damerectoria',
        data:{
            id:id
        }
    }).done(function(data){
        swal({
            title: 'ADVERTENCIA!!!',
            text: "Estas apunto de eliminar la "+data.nombre+". Si lo eliminas afectaras a los puestos y empleados que esten relacionados con ella.",
            type: 'warning',
            showCancelButton: true,
            customClass: 'animated tada',
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Conservar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type:'DELETE',
                    url:'rectorias',
                    data:{
                        id:id
                    }
                }).done(function(data){
                    if(data.status == 200){
                        $("#agregarRectoria").modal('hide');
                        Alertas.exito('Muy bien!',data.mensaje);
                    }else{
                        Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                    }
                }).fail(function(error){
                    Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                });
            }else{
                swal(
                    'Muy bien!',
                    ''+data.nombre+' concervada.',
                    'success'
                )
            }
        });
    }).fail(function(error){
        console.log(error);
    });
});