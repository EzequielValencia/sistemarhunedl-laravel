
$("#guardarPlantel").click(function(){

    let idPlantel = $("#idPlantel").val();
    let nombre = $("#nombre").val();
    let direccion =  $("#direccion").val();
    let email = $("#email").val();
    let telefono = $("#telefono").val();
    let descripcion = $("#descripcion").val();
    let data = {nombre,direccion,telefono,email,descripcion};
    let url = 'planteles/guardar';
    let method = '';
    if(idPlantel==''){
        data.nombre = $("#nombre").val();
        method = 'POST';
    }else{
        data.id = idPlantel;
        data.nombre = $("#nombre").val();
        method = 'PUT';
    }

    $.ajax({
        url:url,
        method:method,
        data:data,
        success:function(data){
            if(data.status == 200){
                Alertas.exito('Muy bien',data.mensaje);
                setTimeout(function () {
                    window.location.reload();
                },2500);
            }
        },error:function(error){

        }
    })


});

$(".btn-warning").click(function(){
    let id = $(this).data('plantelid');

    $.ajax({
        url:'planteles/plantel/'+id,
        method:'GET',
        success:function(data){
            if(data.status == 200){
                $("#nombre").val(data.plantel.nombre);
                $("#idPlantel").val(data.plantel.id);
                $("#direccion").val(data.plantel.direccion);
                $("#email").val(data.plantel.email);
                $("#telefono").val(data.plantel.telefono);
                $("#descripcion").val(data.plantel.descripcion);
                $("#agregarPlantel").modal('show');
            }
        },error:function(error){

        }
    })
});

$("#agregarPlantel").on('hidden.bs.modal',function () {
    $("#idPlantel").val('');
    $("#nombre").val('');
});