var Validador={
    rfcValido:false,
    curpValido:false,
    imssValido:false,
    exiteCoincidencia:false,
    "validaRFC":function(RFC){
        var rfcValido = new RegExp("^(([A-ZÑ&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|" +
            "(([A-ZÑ&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|" +
            "(([A-ZÑ&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|" +
            "(([A-ZÑ&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$");
        return rfcValido.test(RFC);
    },
    "validaCURP":function(CURP){
        var expCurpValido = new RegExp("[A-Z]{1}[AEIOU]{1}[A-Z]{2}"+
            "[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])"+
            "[HM]{1}"+
            "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)"+
            "[B-DF-HJ-NP-TV-Z]{3}"+
            "[0-9A-Z]{1}"+
            "[0-9]{1}");
        return expCurpValido.test(CURP);
    },
    validarIMSS:function(IMSS){
        var imssValido = new RegExp(/^([0-9][1-7])(\d{2})(\d{2})\d{5}$/);
        return imssValido.test(IMSS);
    }
};


function campoInvalido(campo){
    var etiqueta = $(campo).siblings('.control-label');
    var etiquetaError = $(campo).siblings('.error-label');

    $(campo).addClass("invalido");
    etiquetaError.removeClass("hidden");
    etiqueta.addClass("text-danger");
}

function campoValido(campo){
    var etiqueta = $(campo).siblings('.control-label');
    var etiquetaError = $(campo).siblings('.error-label');

    $(campo).removeClass("invalido");
    etiquetaError.addClass("hidden");
    etiqueta.removeClass("text-danger");
}


/**
 * Verifica que el campo CURP este escrito correctamente
 * @returns
 */
$("#curp").focusout(function(){
    var curp = $("#curp").val();
    valido = Validador.validaCURP(curp);
    Validador.curpValido = valido;
    if(!valido){
        campoInvalido("#curp");
    }else{
        buscarCoincidencia($(this).val(),$(this).attr('id'));
        campoValido("#curp");
    }
});

function buscarCoincidencia(valor,campo){
    $.ajax({
        url:'buscarporrfccurpimss',
        type:'get',
        data:{
            campo:campo,
            busqueda:valor,
        },success:function(data){

            if(data.length!=0 && data.id != $("#idEmpleado").val() ){
                Validador.exiteCoincidencia = true;
                if(campo == 'numero_imss'){
                    campo = 'Numero de IMSS';
                }else{
                    campo = campo.toUpperCase();
                }
                swal({
                    type: 'error',
                    title: `Coincidencia en ${campo}`,
                    text: `${valor.toUpperCase()} ya esta registrado en la base de datos`,
                });
            }else{
                Validador.exiteCoincidencia = false;
            }
        },error:function(error){
            console.log(error);
        }
    })
}

/**
 * Verifica que el RFC ingresado sea el correcto
 * @returns
 */
$("#rfc").focusout(function(){
    var rfc = $("#rfc").val();
    var valido = Validador.validaRFC(rfc);
    Validador.rfcValido = valido;
    if(!valido){
        campoInvalido("#rfc");
    }else{
        buscarCoincidencia($(this).val(),$(this).attr('id'));
        campoValido("#rfc");
    }
});
/**
 * Verifica que la informacion introduccida corresponda con un campo valido
 * @returns
 */
$("#numero_imss").focusout(function(){
    var imss = $("#numero_imss").val();
    var valido = Validador.validarIMSS(imss);
    Validador.imssValido = valido;
    if(!valido){
        campoInvalido("#numero_imss");
    }else{
        buscarCoincidencia($(this).val(),$(this).attr('id'));
        campoValido("#numero_imss");
    }
});