let Alertas = {
    'exito':(titulo,text)=>{
        swal({
            position: 'top-end',
            type: 'success',
            title: titulo,
            text:text,
            showConfirmButton: false,
            timer: 2500
        })
    },
    'error':(titulo,text)=>{
        swal(titulo, text,'error');
    }
}

window.onload=function(){
    var tabla = $('#tabla').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });

    tabla.on( 'order.dt search.dt', function () {
        tabla.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

}

function mensajeDeError(titulo,error,footer){
    swal({
        type: 'error',
        title: titulo,
        text: error,
        footer:footer
    })
}

function mensajeExito(mensaje){
    swal({
        position: 'top-end',
        type: 'success',
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
    })
}

function fechaActual(){
    var fechaActual = new Date();
    var diaActual = fechaActual.getDate();
    var mesActual = fechaActual.getMonth()+1;
    var anioActual = fechaActual.getFullYear();

    return anioActual+'-'+((mesActual<10)? '0'+mesActual:mesActual)+'-'+((diaActual<10)? '0'+diaActual:diaActual);
}

function primerDiaDelMes(){
    var fechaActual = new Date();
    var mesActual = fechaActual.getMonth()+1;
    var anioActual = fechaActual.getFullYear();

    return anioActual+'-'+((mesActual<10)? '0'+mesActual:mesActual)+'-'+'01';
}

function validadorDeFechas(fechaInicio,fechaFin){
  var fechaInicio = new Date(fechaInicio);
  var fechaFin = new Date(fechaFin);
  console.log(fechaInicio,fechaFin);
};

/**
 * Habilita o deshabilita el campo fecha de matrimonio
 * por medio del valos seleccionado
 * @returns
 */
$("#estado_civil").change(function(){
    var estadoCivil = $(this).val();
    if(estadoCivil == 'casado'){
        $("#fecha_matrimonio").prop('disabled',false);
        $("#fecha_matrimonio").attr('required','required');
    }else{
        $("#fecha_matrimonio").prop('disabled',true);
        $("#fecha_matrimonio").removeAttr('required','required');
    }

});
/**
 * Determina si los campos de la cedula profesional y el titulo requieren ser activados por
 * la seleccion del grado de estudio
 * @returns
 */
$("#gradoDeEstudio").change(function(){
    var gradoEstudio = $(this).val();
    if(gradoEstudio == 'Licenciatura' || gradoEstudio == 'Maestria' || gradoEstudio == 'Doctorado' ){
        $('#titulo').prop('disabled',false);
        $('#titulo').attr('required','required');
        $('#cedula').prop('disabled',false);
        $('#cedula').attr('required','required');
        $('#situacionActualCedula').prop('disabled',false);
        $('#situacionActualCedula').attr('required','required');
    }else{
        $('#titulo').prop('disabled',true);
        $('#titulo').removeAttr('required','required');
        $('#cedula').prop('disabled',true);
        $('#cedula').removeAttr('required','required');
        $('#situacionActualCedula').prop('disabled',true);
        $('#situacionActualCedula').removeAttr('required','required');
    }
});

/**
 * Ve que rectoria a sido seleccionada y en base a esa seleccion llena el campo cargo
 */
$("#rectoria").change(async function(){
    var idRectoria = $(this).val();
    console.log(idRectoria);
    if(idRectoria!=0){
        var respuesta = await PuestosProvider.porRectoria(idRectoria);
        var opciones = '<option value="0">Seleccione un puesto</option>';
        if(respuesta.status=='OK'){
            $("#cargo").empty();
            var puestos = respuesta.data;
            puestos.forEach(function(element){
                opciones+='<option value="'+element.id+'">'+element.nombre+'</option>';
            });
            $("#cargo").append(opciones);
            $("#cargo").prop('disabled',false);
        }else{
            mensajeDeError('Ocurrio un error','Existe un error con el sistema','contacte con su administrador');
        }
    }else{
        $("#cargo").append('<option value="0">Seleccione un puesto</option>');
        $("#cargo").prop('disabled',true);
    }

});


$("#menu-toggle").click(function(){
    setTimeout(() => {

        if (typeof(Storage) !== "undefined") {
            console.log($('body').attr('class'));
            localStorage.setItem('classBody',$('body').attr('class'));
        } else {
            console.log("no");
        }
    }, 500);
});

$( document ).ready(function() {
    var classBody = localStorage.getItem('classBody');
    if(classBody!==null){
        $("body").removeClass().addClass(classBody);
    }
});

function readURL(input) {
    console.log(input.files[0]);
    if(!(((input.files[0].size/1024)/1024)>2)){
        var t = input.files[0].type.split('/').pop().toLowerCase();
        if(t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif"){
            Alertas.error('Archivo no valido','Solo puedes subir imagenes')
        }else{
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#verImagen').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }

    }else{
       Alertas.error('Imagen demasiado grande','Solo puedes subir imagenes menores a 2 Megas de tamaño')
    }
}

$("#imagenProfesor").change(function() {
    readURL(this);
});

/***
 * Retorna un
 * @param $form
 */
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}