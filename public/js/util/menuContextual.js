$(function() {

    var $contextMenu = $("#contextMenu");
    let idEmpleado;
    $("body").on("contextmenu", "table tr", function(e) {
        let verEmpleado = $("#verInfoEmpleado").attr('href');
//        verEmpleado+="?id="+$(this).data('empleadoid');
        idEmpleado = $(this).data('empleadoid');
//        $("#verInfoEmpleado").attr('href',verEmpleado);
        $contextMenu.css({
            display: "block",
            left: e.pageX,
            top: e.pageY
        });
        return false;
    });

    $('body').on('click',function(event){
        if($contextMenu.css('display')!='none'){
            event.preventDefault();
            $contextMenu.css({
                display: "none",
                left: 0,
                top: 0
            });
            console.log('menu desplegado');
        }
    });
    $("#verInfoEmpleado").click(function(){
        window.location.href = urlVerEmpleados+'?id='+idEmpleado;
    });

    $("#eliminarEmpleado").click(function(){

        $.ajax({
            url: 'empleado',
            type:'GET',
            data:{
                id:idEmpleado
            },
            success:function(data){
                console.log(data);
                swal({
                    title: 'ADVERTENCIA!!!',
                    text: "Estas apunto de eliminar a "+data.nombre+". Si lo eliminas afectaras la información relacionada con el/la emplead@.",
                    type: 'warning',
                    showCancelButton: true,
                    customClass: 'animated tada',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Conservar'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: 'eliminar',
                            type: 'DELETE',
                            data:{
                                id:idEmpleado,
                                _token:$('meta[name="csrf-token"]').attr('content')
                            }
                        }).done(function(data){
                            if(data.status == 200){
                                $("#agregarRectoria").modal('hide');
                                Alertas.exito('Muy bien!',data.mensaje);
                            }else{
                                Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                            }
                        }).fail(function(error){
                            Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                        });
                    }else{
                        swal(
                            'Muy bien!',
                            ''+data.nombre+' concervad@.',
                            'success'
                        )
                    }
                });
            },
            error:function(error){
                console.log(error);
            }
        })

    });

    $contextMenu.on("click", "a", function() {
        $contextMenu.hide();
    });

});
