$("#guardar").click(function(){
    let nombre = $("#nombre").val();
    let id = $("#idCarrera").val();
    let codigo = $("#codigo").val();
    let gradoEstudio = $("#gradoEstudio").val();
    console.log(id=='');
    if(id==''){//Si no tiene un id, entonces es un registro nuevo
        $.ajax({
            url:'',
            type:'POST',
            data:{
                codigo:codigo,
                gradoEstudio:gradoEstudio,
                nombre:nombre
            },
            success:function (data) {
                if(data.status==200){
                    Alertas.exito('Muy bien!!',data.mensaje)
                    setTimeout(function(){window.location.reload();},2500);

                }else{
                    Alertas.error('Uuuupss!!',data.mensaje);
                }
            },error:function(error){

            }
        })
    }else{//Si tiene id, es actualizacion de registro
        $.ajax({
            url:'',
            type:'PUT',
            data:{
                id:id,
                codigo:codigo,
                gradoEstudio:gradoEstudio,
                nombre:nombre
            },
            success:function (data) {
                if(data.status==200){
                    Alertas.exito('Muy bien!!',data.mensaje)
                    setTimeout(function(){window.location.reload();},2500);

                }else{
                    Alertas.error('Uuuupss!!',data.mensaje);
                }
            },error:function(error){

            }
        })
    }
});

$("#agregarCarrera").on('hidden.bs.modal', function (e) {
    $("#nombre").val('');
    $("#idCarrera").val('');
    $("#codigo").val('');
    $("#gradoEstudio").val('');
})

$(".editarCarrera").click(function(){
    let id=$(this).data('id_carrera');
    console.log(id);
    $.ajax({
        url:'carrerascursos/carrera',
        type:'GET',
        data:{
            id:id
        },success:function(data){
            $("#nombre").val(data.nombre);
            $("#idCarrera").val(data.id);
            $("#codigo").val(data.codigo);
            $("#gradoEstudio").val(data.gradoEstudio);
            $("#agregarCarrera").modal('show');
        },error:function(){

        }
    });
});

$(".eliminarCarrera").click(function(){
    let id=$(this).data('id_carrera');
    console.log(id);
    $.ajax({
        url:'carrerascursos/carrera',
        type:'GET',
        data:{
            id:id
        },success:function(data){
            swal({
                title: 'Cuidado!!!',
                text: `Si eliminas a ${data.nombre} eliminaras todos los registros que este relacionados con el.`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Elminar',
                cancelButtonText:'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:'',
                        type:'DELETE',
                        data:{
                            id:data.id
                        },success:function(data){
                            if(data.status == 200){
                                Alertas.exito('Muy bien!!',data.mensaje);
                                setTimeout(function () {
                                    window.location.reload();
                                },2500);

                            }else{
                                Alertas.error('Uuups',data.mensaje);
                            }
                        },error:function(data){
                            Alertas.error('Uuups',data.mensaje);
                        }
                    })
                }
            })
        },error:function(){

        }
    });
});