$("#guardarNuevoUsuario").click(function(){
    let inputs = $("#formNuevoUsuario :input[required]");
    for(let i=0;i<inputs.length;i++){
        if($(inputs[i]).val()==''){
            console.log($(inputs[i]));
            Alertas.error('Campos incompletos','Verifica que la información este completa');
            return;
        }
    }
    if($("#password").val()!==$("#confirmPassword").val()){
        Alertas.error('Error de password','Verifica que las contraseñas coincidan');
        return;
    }

    console.log($("#role").val());
    if($("#idUsuario").val()==''){
        let usuario={
            user:$("#user").val(),
            nombre:$("#nombre").val(),
            paterno:$("#paterno").val(),
            materno:$("#materno").val(),
            role:$("#role").val(),
            password:$("#password").val()
        }
        $.ajax({
            url:'',
            type:'POST',
            data:usuario,
            success:function(data){
                if(data.status == 200){
                    setTimeout(function () {
                        window.location.reload()
                    },2000);
                    Alertas.exito('Muy bien',data.mensaje);
                }else{
                    Alertas.error('Upps',data.mensaje);
                }
            },
            error:function(error){
                console.log(error);
            }
        });
    }else{
        let usuario={
            id:$("#idUsuario").val(),
            user:$("#user").val(),
            nombre:$("#nombre").val(),
            paterno:$("#paterno").val(),
            materno:$("#materno").val(),
            role:$("#role").val(),
            password:$("#password").val()
        }
        $.ajax({
            url:'',
            type:'PUT',
            data:usuario,
            success:function(data){
                if(data.status == 200){
                    setTimeout(function () {
                        window.location.reload()
                    },2000);
                    Alertas.exito('Muy bien',data.mensaje);
                }else{
                    Alertas.error('Upps',data.mensaje);
                }
            },
            error:function(error){
                console.log(error);
            }
        });
    }


});

$(".activarDesactivarUsuario").click(function(){
   let id = $(this).data('user_id');
   $.ajax({
       url:'usuarios/usuario',
       type: 'GET',
       data:{id:id},
       success:function(data){
           let titulo = data.activo == 1 ? `Realmente quieres desactivar a ${data.nombre} ${data.paterno} ${data.materno}`:
                                           `Realmente quieres reactivar a ${data.nombre} ${data.paterno} ${data.materno}`;
           let texto = data.activo == 1 ? 'Si lo desactivas no podra tener acceso al sistema':'Si lo activas, volvera a tener acceso al sistema';

           let textoBoton = data.activo == 1 ? 'Sí desacativar':'Sí reactivar';
           swal({
               title: titulo,
               text: texto,
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: textoBoton,
               cancelButtonText:'Cancelar'
           }).then((result) => {
               if (result.value) {
                   data.activo = !data.activo;
                   $.ajax({
                       url:'',
                       type:'PUT',
                       data:data,
                       success:function(data){
                           if(data.status==200){
                               Alertas.exito('Muy bien!',data.mensaje);
                           }else{
                               Alertas.error('Uuups!!','ocurrio un error al ediar el usuario');
                           }
                       },
                       error:function(){
                           Alertas.error('Uuups!!','ocurrio un error al ediar el usuario');
                       }
                   })
               }
           })
       },
       error:function(error){
           Alertas.error('Uuups!!','ocurrio un error al procesar su petición');
       }
   })
});

$(".cambiarContrasena").click(function() {
    let idUsuario = $(this).data('user_id');
    swal({
        title: "Cuidado. Estas apunto de cambiar el password",
        text: "Si cambias la contraseña de este usuario es posible que no pueda acceder al sistema",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Cambiar contraseña',
        cancelButtonText:'Cancelar'
    }).then((result)=>{
        if(result.value){
            $("#cambiarContraseña").modal('show');
            $("#usuarioParaCambiarPassword").val(idUsuario);
        }
    });
});

$("#guardarUsuario").on('hidden.bs.modal', function (e) {
    $("#idUsuario").val('');
    $("#nombre").val('');
    $("#paterno").val('');
    $("#materno").val('');
    $("#user").val('');
    $("#role").val('');
    $("#password").prop('disabled',false);
    $("#confirmPassword").prop('disabled',false);
    $("#password").prop('required',true);
    $("#confirmPassword").prop('required',true);
});

$(".editar").click(function(){
    var idUsuario = $(this).data('user_id');
    $.ajax({
        url:'usuarios/usuario',
        type:'GET',
        data:{
            id:idUsuario
        },
        success:function (data) {
            if(data.status==200){
                var usuario = data.data;
                $("#idUsuario").val(usuario.id);
                $("#nombre").val(usuario.nombre);
                $("#paterno").val(usuario.paterno);
                $("#materno").val(usuario.materno);
                $("#user").val(usuario.user_name);
                $("#role").val(usuario.role);
                $("#password").prop('disabled',true);
                $("#confirmPassword").prop('disabled',true);
                $("#password").prop('required',false);
                $("#confirmPassword").prop('required',false);
                $("#guardarUsuario").modal('show');
            }else{
                Alertas.error('Usuario no encontrado','No se a podido localizar al usuario');
                console.log(data);
            }
        },error:function (error){
            Alertas.error('Error','Ocurrio un error al momento de procesar tu solicitud');
            console.log(error)
        }
    })
});


$("#guardarNuevoPassword").click(function(){

    let inputs = $("#formCambiarPassword :input[required]");
    let id=$("#usuarioParaCambiarPassword").val();
    for(let i=0;i<inputs.length;i++){
        if($(inputs[i]).val()==''){
            console.log($(inputs[i]));
            Alertas.error('Campos incompletos','Verifica que la información este completa');
            return;
        }
    }
    let passwordNuevo = $("#passwordNuevo").val();
    let confirmPassword = $("#confirmPasswordNuevo").val();
    if(passwordNuevo!=confirmPassword){
        Alertas.error('Uuuups!!','Los passwords no coinciden. Verifica que sean iguales');
        return;
    }


    $.ajax({
        url:'usuarios/cambiarpassword',
        type:'POST',
        data: {
            id: id,
            password: passwordNuevo
        },
        success:function(data){
            if(data.status==200){
                Alertas.exito('Muy bien!',data.mensaje);
            }else{
                Alertas.error('Uuups!!','ocurrio un error al ediar el usuario');
            }
        },error:function(){
            Alertas.error('Uuups!!','ocurrio un error al procesar su petición');
        }
    })
});

$(".eliminarUsuario").click(function(){
   let id = $(this).data('user_id');

   $.ajax({
       url:'usuarios/usuario',
       type:'GET',
       data:{
           id:id
       },
       success:function(data){
           if(data.status==200){
               var data = data.data;
               swal({
                   title: `Relamente quieres eliminar a ${data.nombre} ${data.paterno} ${data.materno}`,
                   text: 'Si eliminas a este usuario, no podras recuperarlo',
                   type: 'warning',
                   showCancelButton: true,
                   confirmButtonColor: '#3085d6',
                   cancelButtonColor: '#d33',
                   confirmButtonText: 'Eliminar',
                   cancelButtonText:'Cancelar'
               }).then((result) => {
                   if (result.value) {
                       data.activo = !data.activo;
                       $.ajax({
                           url:'',
                           type:'DELETE',
                           data:data,
                           success:function(data){
                               if(data.status==200){
                                   setTimeout(function(){
                                       window.location.reload();
                                   },2000);
                                   Alertas.exito('Muy bien!',data.mensaje);
                               }else{
                                   Alertas.error('Uuups!!','ocurrio un error al ediar el usuario');
                               }
                           },
                           error:function(){
                               Alertas.error('Uuups!!','ocurrio un error al ediar el usuario');
                           }
                       })
                   }
               });
           }else{
               Alertas.error('Uuups!!','Ocurrio un error al localizar el usuario');
               console.log(data);
           }
       },
       error:function(error){
           Alertas.error('Uuups!!','Ocurrio un error al procesar su petición');
           console.log(error);
       }
   })
});