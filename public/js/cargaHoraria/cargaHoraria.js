let cargaHoraria=[];

/***
 * Al momento de cambiar el selector de las carreras y cursos, busca la materias que tiene dicho curso y los cambia.
 */
window.onload = function(){
    $.ajax({
        url:urlPrincipal+'/horario/empleado/'+idEmpleado,
        type: 'GET',
        success:function (data) {
            cargaHoraria = data;
        },error:function(error){
            console.log(error);
        }
    })
}
$("#carreraCurso").change(function(){
   let idCarreraCurso = $(this).val();
   $.ajax({
       url:urlPrincipal+'/materias/curso/'+idCarreraCurso,
       type:'GET',
       success:function(data){
           if(data.length>=1){
                $("#materia").prop('disabled',false);
                $("#materia").empty();
                $("#materia").append("<option value='0'>Selecciona una materia</option>");
                data.forEach(function(e,i){
                   $("#materia").append("<option value='"+e.id+"'>"+e.nombre+"</option>");
                });
               $("#agregarCargaHoraria").prop('disabled',true);
           }else{
               $("#materia").empty();
               $("#materia").prop('disabled',true);
               $("#agregarCargaHoraria").prop('disabled',false);
           }
       },error:function(data){
           console.log(data);
       }
   });
});

$("#materia").change(function(){
    let materiaSeleccionada = $(this).val();
    if(materiaSeleccionada!=0){
        $("#agregarCargaHoraria").prop('disabled',false);
    }else{
        $("#agregarCargaHoraria").prop('disabled',true);
    }
})
/***
 * Reinicia todos los selectores y el input de la caja horaria
 */
$("#reiniciarSelectores").click(function () {
    $("#carreraCurso").val('');
    $("#materia").empty();
    $("#cargaHoraria").val('');
    $("#agregarCargaHoraria").prop('disabled',true);
});

$("#agregarCargaHoraria").click(function(){
    let id_curso = $("#carreraCurso").val();
    let carrera = $("#carreraCurso option:selected").text();
    let id_materia = $("#materia").val();
    let materia = $("#materia option:selected").text();
    let horas = $("#cargaHoraria").val();
    let id_empleado = idEmpleado;
    let coincidencia = false;
    if(horas > 0){


        cargaHoraria.forEach(function(e,i){
            if(e.id_curso == id_curso && id_materia == e.id_materia){
                e.horas += parseFloat(horas);
                coincidencia = true;
                return;
            }
        });
        if(!coincidencia){
            cargaHoraria.push({id_curso,id_materia,id_empleado,carrera,materia,horas:parseFloat(horas)});
        }

        recalcularHorasDeClase();
        $("#reiniciarSelectores").click();
    }else{
        Alertas.error('Horas de clase no validas','No puedes tener horas de clase en cero o negativas');
    }

});

function cambiarCargaHoraria(index,elemento){
    cargaHoraria[index].horas = parseFloat($(elemento).val());
    recalcularHorasDeClase();
}

function recalcularHorasDeClase(){
    let totalHoras = 0;
    $("#tablaCargaHoraria tbody").empty();

    cargaHoraria.forEach(function(e,i){
        $("#tablaCargaHoraria tbody").append(
            "<tr>" +
            "<td class='p-2'>"+e.carrera+"</td>" +
            "<td class='p-2'>"+e.materia+"</td>" +
            "<td class='p-2'>"+
            "<input type='number' class='validate form-control form-control-sm cargaHoraria'"+
                "placeholder='Carga Horaria'  value='"+e.horas+"' onchange='cambiarCargaHoraria("+i+",this)' min='1'/>"
            +"</td>" +
            "<td class='text-center'>" +
            "<button type='button' class='btn btn-danger btn-sm' onclick='quitarCargaHoraria("+i+")'>" +
                "<i class='fa fa-eraser'></i>" +
            "</button>" +
            "</td>"+
            "</tr>");
        totalHoras += e.horas;
    });
    $("#totalHoras").text("Horas Totales: "+totalHoras);
}

function quitarCargaHoraria(indice){
    cargaHoraria.splice(indice,1);
    recalcularHorasDeClase();
}

$("#guardarCargaHoraria").click(function(){
    $.ajax({
        url:urlPrincipal+'/horario/crearHorario',
        type:'POST',
        data:{
            id_empleado : idEmpleado,
            carga_horaria:cargaHoraria
        },
        success:function(data){
            if(data.status == 200){
                Alertas.exito("Muy bien!!!",data.mensaje);
            }
        },error:function(error){

        }
    });
});