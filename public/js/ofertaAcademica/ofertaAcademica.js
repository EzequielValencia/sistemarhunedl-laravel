$("#guardar").click(function(){
   let nombre = $("#nombre").val();
   let codigo = $("#codigo").val();
   let idGrado = $("#idGradoEstudio").val();

   if(idGrado==''){
       $.ajax({
           url:'',
           type:'POST',
           data:{
               nombre:nombre,
               codigo:codigo
           },
           success:function(data){
               if(data.status == 200){
                   Alertas.exito('Muy bien!!',data.mensaje);
               }else{
                   Alertas.error('Uuups',data.mensaje);
               }
           },error:function(data){
               Alertas.error('Uuups!!','Ocurrio un error');
           }
       })
   }else{
       $.ajax({
           url:'',
           type:'PUT',
           data:{
               nombre:nombre,
               codigo:codigo,
               id:idGrado
           },
           success:function(data){
               if(data.status == 200){
                   Alertas.exito('Muy bien!!',data.mensaje);
                   setTimeout(function () {
                       window.location.reload();
                   },2500);

               }else{
                   Alertas.error('Uuups',data.mensaje);
               }
           },error:function(data){
               Alertas.error('Uuups!!','Ocurrio un error');
           }
       })
   }
});

$(".editarGradoEstudio").click(function(){
    let id = $(this).data('grado_id');
    $.ajax({
        url:'ofertaacademica/gradoEstudio',
        type:'GET',
        data:{
            id:id
        },success:function(data){
            $("#nombre").val(data.nombre);
            $("#codigo").val(data.codigo);
            $("#idGradoEstudio").val(data.id);
            $("#agregarGradoEstudio").modal('show');
        }
    })
});

$(".eliminarGradoEstudio").click(function(){
    let id = $(this).data('grado_id');
    $.ajax({
        url:'ofertaacademica/gradoEstudio',
        type:'GET',
        data:{
            id:id
        },success:function(data){
            swal({
                title: 'Cuidado!!!',
                text: `Si eliminas a ${data.nombre} eliminaras todos los registros que este relacionados con el.`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Elminar',
                cancelButtonText:'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:'',
                        type:'DELETE',
                        data:{
                            id:data.id
                        },success:function(data){
                            if(data.status == 200){
                                Alertas.exito('Muy bien!!',data.mensaje);
                                setTimeout(function () {
                                    window.location.reload();
                                },2500);

                            }else{
                                Alertas.error('Uuups',data.mensaje);
                            }
                        },error:function(data){
                            Alertas.error('Uuups',data.mensaje);
                        }
                    })
                }
            })
        },error:function (error) {
            Alertas.error('Uuups',error.mensaje);
        }
    })
});