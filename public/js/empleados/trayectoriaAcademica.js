let trayectoriaAcademica = [];
let metodoParaGuardar;
/***
 *
 */
$("#formTrayectoriaAcademica").submit(function(event){
    event.preventDefault();
    let institucion = $("#institucion").val();
    let grado_de_estudio = $("#grado_de_estudio").val();
    let titulo = $("#titulo").val();
    let cedula = $("#cedula").val();
    let situacion_cedula = $("#situacionActualCedula").val();
    $("#titulo").prop('disabled',true);
    $("#cedula").prop('disabled',true);
    $("#situacionActualCedula").prop('disabled',true);
    trayectoriaAcademica.push({
        institucion,
        grado_de_estudio,
        titulo,
        cedula,
        situacion_cedula
    });
    agregaTrayectoriaATabla();
    $("#formTrayectoriaAcademica")[0].reset();
});

/***
 *
 */
function agregaTrayectoriaATabla(){
    cuerpoTabla = '';
    $("#tablaTrayectoriaAcademica tbody").empty();
    trayectoriaAcademica.forEach(function(estudio,index){

        cuerpoTabla+=`<tr>
                        <td>${estudio.institucion}</td>
                        <td>${estudio.grado_de_estudio}</td>
                        <td>${estudio.titulo==null?'':estudio.titulo}</td>
                        <td>${estudio.cedula==null?'':estudio.cedula}</td>
                        <td>${estudio.situacion_cedula==null?'':estudio.situacion_cedula}</td>
                        <td>
                            <button class="btn btn-sm btn-danger" onclick="eliminarDetrayectoria(${index})">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>`;

    });
    $("#tablaTrayectoriaAcademica tbody").append(cuerpoTabla);
}

/***
 *
 * @param indice
 */
function eliminarDetrayectoria(indice){
    trayectoriaAcademica.splice(indice, 1);
    agregaTrayectoriaATabla();
}

$("#guardarTrayectoria").click(function(){
    $.ajax({
        url:'guardatrayectoria',
        method:metodoParaGuardar,
        data:{
            trayectoriaAcademica,
            idEmpleado:$("#idEmpleado").val()
        },
        success:function(data){
            if(data.status == 200){
                Alertas.exito('Muy bien!!',data.mensaje);
                setTimeout(function () {
                   window.location.reload();
                },200);
            }
        },error:function(){

        }
    })
});

$("#agregarEstudios").click(function(){
    $.ajax({
        url:'estudios/'+$("#idEmpleado").val(),
        method:'GET',
        success:function(data){
            if(data.status==200){
                trayectoriaAcademica = data.data;
                if(trayectoriaAcademica.length>0){
                    metodoParaGuardar = 'PUT';
                    agregaTrayectoriaATabla();
                    $("#modalAgregarEstudios").modal('show');
                }else{
                    metodoParaGuardar='POST';
                    $("#modalAgregarEstudios").modal('show');
                }
            }else{
                Alertas.error('Upsss',data.mensaje);
            }
        },error:function(){
            Alertas.error('Upsss','Ocurrio un error inesperado');
        }
    })
});

$(".editEstudio").click(function(){
    let idEstudio = $(this).data('idestudio');

    $.ajax({
        url:'estudios/estudio/'+idEstudio,
        method:'GET',
        success:function(data){
            if(data.status==200){
                let estudio = data.data;
                $("#idEstudioEdit").val(estudio.id);
                $("#institucionEdit").val(estudio.institucion);
                $("#grado_de_estudio_edit").val(estudio.grado_de_estudio);
                if(estudio.grado_de_estudio == 'Licenciatura' || gradoEstudio == 'Maestria' || gradoEstudio == 'Doctorado'){
                    $("#tituloEdit").prop('disabled',false);
                    $("#cedulaEdit").prop('disabled',false);
                    $("#situacionActualCedulaEdit").prop('disabled',false);
                }
                $("#tituloEdit").val(estudio.titulo);
                $("#cedulaEdit").val(estudio.cedula);
                $("#situacionActualCedulaEdit").val(estudio.situacion_cedula);
                $("#modalEditarEstudio").modal('show');
            }else{
                Alertas.error('Upsss',data.mensaje);
            }
        },error:function(){
            Alertas.error('Upsss','Ocurrio un error inesperado');
        }
    })

});

$("#guardarTrayectoriaEdit").click(function(){

    $.ajax({
        url:'estudios/editar',
        method:'PUT',
        data:getFormData($("#formTrayectoriaAcademicaEditar")),
        success:function(data){
            if(data.status == 200){
                $("#modalEditarEstudio").modal('hide');
                Alertas.exito('Muy bien!!',data.mensaje);
                setTimeout(function () {
                    window.location.reload();
                },200);
            }else{
                Alertas.error('Upps',data.mensaje);
            }
        },error:function(error){
            Alertas.error('Upps','Ocurrio un error al momento de procesar tu solicitud');
            console.log(error);
        }
    });

});


$(".eliminarEstudio").click(function(){
    $.ajax({
        url:'/empleados/estudios/estudio/'+$(this).data('idestudio'),
        method:'GET',
        success:function(data){
            if(data.status == 200){
                let estudio = data.data;
                Swal({
                    title: `Seguro que quieres eliminar ${estudio.titulo}`,
                    text: "Si lo haces no se podra recuperar esta información",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Elimnar',
                    cancelButtonText:'Cancelar'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url:'/empleados/estudios/eliminar/'+estudio.id,
                            method:'DELETE',
                            success:function(data){
                                if(data.status == 200){
                                    Alertas.exito('Muy bien!!',data.mensaje);
                                    setTimeout(function () {
                                        window.location.reload();
                                    },200);
                                }else{
                                    Alertas.error('Upps',data.mensaje);
                                }
                            },error:function(error){
                                Alertas.error('Upps','Ocurrio un error al momento de procesar tu solicitud');
                                console.log(error);
                            }
                        });
                    }
                })
            }else{
                Alertas.error('Upps',data.mensaje);
            }
        },error:function(){

        }
    })
});