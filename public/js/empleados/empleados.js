$("#eliminarEmpleado").click(function(){
    let idEmpleado = $("#idEmpleado").val();
    let nombre = $("#nombre").val();
    let apellidoPaterno = $("#apellidoPaterno").val();
    let apellidoMaterno = $("#apellidoMaterno").val();
            swal({
                title: 'ADVERTENCIA!!!',
                text: `Estas apunto de eliminar a ${nombre} ${apellidoPaterno} ${apellidoMaterno}. Si lo eliminas afectaras la información relacionada con el/la emplead@.`,
                type: 'warning',
                showCancelButton: true,
                customClass: 'animated tada',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Conservar'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: 'eliminar',
                        type: 'DELETE',
                        data:{
                            id:idEmpleado,
                            _token:$('meta[name="csrf-token"]').attr('content')
                        }
                    }).done(function(data){
                        if(data.status == 200){
                            Alertas.exito('Muy bien!',data.mensaje);
                        }else{
                            Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                        }
                    }).fail(function(error){
                        Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                    });
                }else{
                    swal(
                        'Muy bien!',
                        ''+data.nombre+' concervada.',
                        'success'
                    )
                }
            });
});

/***
 * Boton para dar de baja al empleado seleccionado
 *
 */
$("#darDeBajaEmpleado").click(function () {
    let idEmpleado = $("#idEmpleado").val();
    let nombre = $("#nombre").val();
    let apellidoPaterno = $("#apellidoPaterno").val();
    let apellidoMaterno = $("#apellidoMaterno").val();
    swal({
        title: 'ADVERTENCIA!!!',
        text: `Estas apundo de dar de baja a ${nombre} ${apellidoPaterno} ${apellidoMaterno}. Si lo haces afectaras la información relacionada con el/la emplead@.`,
        type: 'warning',
        showCancelButton: true,
        customClass: 'animated tada',
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Dar de baja',
        cancelButtonText: 'Conservar'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: 'dardebaja',
                type: 'PUT',
                data:{
                    id:idEmpleado,
                    _token:$('meta[name="csrf-token"]').attr('content')
                }
            }).done(function(data){
                if(data.status == 200){
                    Alertas.exito('Muy bien!',data.mensaje);
                    setTimeout(function(){
                        window.location.reload();
                    },2000);
                    $("[type = submit]").prop('disabled', true);
                }else{
                    Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                }
            }).fail(function(error){
                Alertas.error('Uups!','Ocurrio un error al guardar el registro');
            });
        }else{
            swal(
                'Muy bien!',
                ''+data.nombre+' concervada.',
                'success'
            )
        }
    }
    );
});

$("#restaurarEmpleado").click(function () {
    let idEmpleado = $("#idEmpleado").val();
    let nombre = $("#nombre").val();
    let apellidoPaterno = $("#apellidoPaterno").val();
    let apellidoMaterno = $("#apellidoMaterno").val();
    swal({
        title: 'ADVERTENCIA!!!',
        text: `Estas apundo de restaurar a ${nombre} ${apellidoPaterno} ${apellidoMaterno}. Esto restaurara el/la emplead@.`,
        type: 'info',
        showCancelButton: true,
        customClass: 'animated tada',
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Restaurar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: 'restaurarempleado',
                    type: 'PUT',
                    data:{
                        id:idEmpleado,
                        _token:$('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function(data){
                    if(data.status == 200){
                        Alertas.exito('Muy bien!',data.mensaje);
                        setTimeout(function(){
                            window.location.reload();
                        },2000);
                        $("[type = submit]").prop('disabled', true);
                    }else{
                        Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                    }
                }).fail(function(error){
                    Alertas.error('Uups!','Ocurrio un error al guardar el registro');
                });
            }else{
                swal(
                    'Muy bien!', `${nombre} ${apellidoPaterno} ${apellidoMaterno} sin cambios`,
                    'info'
                )
            }
        }
    );
});

$("#cargo").change(function(){
   let puestosSeleccionados = $(this).val();
   console.log(puestosSeleccionados);
   $.ajax({
       url:'jefesporpuestosubordinado',
       type:'GET',
       data:{
           puestosSeleccionados
       }
   }).done(function(data){
       let opcion = '<option >Selecciona al jefe del empleado</option>';
      if(data.length>=1){
        $("#jefe").empty();
        data.forEach(function(element,i){
           opcion += `<option value="${element.id}">${element.nombre} ${element.apellido_paterno} ${element.apellido_materno}</option>`;
        });
        $("#jefe").prop('disabled',false);
        $("#jefe").append(opcion);
      }else{
          $("#jefe").empty();
          $("#jefe").prop('disabled',true);
      }
   }).fail(function(){
       Alertas.error('Uups!','Ocurrio un error.');
   });
});

$("#crearEmpleado").submit(function(event){
    if(Validador.curpValido && Validador.rfcValido && Validador.imssValido && !Validador.exiteCoincidencia){

    }else{
        event.preventDefault();
        swal('Campos invalidos','Verifica que la información en los campos: RFC, CURP y Numero de IMSS este correcta','danger');
    }
});

$("#grado_de_estudio").change(function () {
    let gradoEstudio = $(this).val();
    if(gradoEstudio == 'Licenciatura' || gradoEstudio == 'Maestria' || gradoEstudio == 'Doctorado'){
        $("#titulo").prop('disabled',false);
        $("#cedula").prop('disabled',false);
        $("#situacionActualCedula").prop('disabled',false);
    }else{
        $("#titulo").prop('disabled',true);
        $("#cedula").prop('disabled',true);
        $("#situacionActualCedula").prop('disabled',true);
    }
});

