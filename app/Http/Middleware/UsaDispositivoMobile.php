<?php

namespace PeopleUnedl\Http\Middleware;

use PeopleUnedl\Accesos;
use Closure;
use Jenssegers\Agent\Agent;

/***
 * @author Ezequiel Valencia Moreno 6A turno Mixto
 * @version 28 sept 2018
 * Class UsaDispositivoMobile Filtro para verificar desde donde esta accediendo el usuario y agregarlo a la
 * base de datos. Se verificara si se permitira la entrar a dispositivos mobiles
 * @package PeopleUnedl\Http\Middleware
 */
class UsaDispositivoMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agente = new Agent();//Clase pasa conseguir la informacion desde donde se accede al sistema


        $acceso = new Accesos();
        $acceso->hora_acceso = now();
        $acceso->dispositivo = $agente->device();
        $acceso->sistema_operativo = $agente->platform();
        $acceso->navegador = $agente->browser();
        $acceso->permitido = true;
        $acceso->id_usuario = \Auth::user()->id;
        $acceso->save();
        /*if($agente->isMobile()){
            dd('No se puede usar un celular');
            redirect('/logout');
        }*/
        return $next($request);
    }
}
