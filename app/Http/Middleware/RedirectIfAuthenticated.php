<?php

namespace PeopleUnedl\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use PeopleUnedl\Logs;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $log = new Logs();

        if (!Auth::guard($guard)->check()) {

            $log->mensaje="Intento de entrar en ".\Request::url()." sin estar loguead ";
            $log->user_id=null;
            $log->save();
            return redirect('/');
        }else{
            $parametros = http_build_query($request->except('password'));
            $log->mensaje="Acceso a  ".\Request::url();
            $log->user_id = Auth::id();
            $log->save();
            return $next($request);
        }
       return $next($request);
    }
}
