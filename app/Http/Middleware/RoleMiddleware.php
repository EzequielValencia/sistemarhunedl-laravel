<?php

namespace PeopleUnedl\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use PeopleUnedl\Logs;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = Auth::user();
        $prefix = $request->route()->getPrefix();

        if($prefix === '/usuarios' && $usuario->role !== 'admin'){
            $log = new Logs();

            $log->mensaje="$usuario->nombre $usuario->paterno $usuario->materno Intento acceder sin autorización";
            $log->user_id = Auth::id();
            $log->save();
            return redirect('/');
        }

        return $next($request);
    }
}
