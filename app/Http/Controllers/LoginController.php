<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use PeopleUnedl\Accesos;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Response;
use PeopleUnedl\Logs;

class LoginController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware',['except'=>['index','login']]);
    }

    public function index(){
        if(\Auth::check()){
            return view('principalLayout',['vicerrectorias'=>$this->vicerrectorias]);
        }else{
            return view('login');
        }
    }

    public function login(Request $request){

        $user = $request->get('usuario');
        $password = $request->get('password');
        if(\Auth::attempt(['user_name' => $user, 'password' => $password,'activo'=>true])){
            $usuario = \Auth::user();
            $agente = new Agent();//Clase pasa conseguir la informacion desde donde se accede al sistema
            $acceso = new Accesos();
            $log    = new Logs();
            $log->mensaje = "accedio al sistema $usuario->nombre $usuario->paterno $usuario->materno";
            $acceso->hora_acceso = now();
            $acceso->dispositivo = $agente->device();
            $acceso->sistema_operativo = $agente->platform();
            $acceso->navegador = $agente->browser();
            $acceso->permitido = $agente->isDesktop();
            $acceso->id_usuario = $usuario->id;
            $acceso->ip = \Request::ip();
            $acceso->es_escritorio = $agente->isDesktop();
            $acceso->es_tablet = $agente->isTablet();
            $acceso->es_celular = $agente->isMobile();
            $acceso->es_android = $agente->isAndroidOS();
            $acceso->es_iphone = $agente->is('iPhone');
            $acceso->save();
            if(!$agente->isDesktop()){
                \Auth::logout();

                return  view('login',['error'=>true,'mensaje'=>"No puedes acceder desde tu celular o tablet"]);
            }
            return redirect('/');
        }else{
            $error = true;
            return  view('login',['error'=>$error]);
        }
    }

    public function loginOut(){
        \Auth::logout();
        return redirect('/');
    }
}
