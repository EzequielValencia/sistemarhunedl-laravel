<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Support\Facades\Response ;
use Symfony\Component\HttpFoundation\Response as ResponseStatus;
use PeopleUnedl\Estudio;
use PeopleUnedl\Empleado;
use Illuminate\Http\Request;

class EstudioController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        $trayectoriaAcademica = $request->trayectoriaAcademica;
        $idEmpleado = $request->idEmpleado;

        foreach ($trayectoriaAcademica as $estudioEmpleado){
            $estudio = new Estudio();
            $estudio->empleado_id = $idEmpleado;
            $estudio->institucion = $estudioEmpleado['institucion'];
            $estudio->grado_de_estudio = $estudioEmpleado['grado_de_estudio'];
            $estudio->titulo = $estudioEmpleado['titulo'];
            $estudio->cedula = $estudioEmpleado['cedula'];
            $estudio->situacion_cedula = $estudioEmpleado['situacion_cedula'];
            $estudio->save();
        }

        return array('status'=>ResponseStatus::HTTP_OK,'mensaje'=>'Trajectoria academica guardada correctamente');
    }


    public function editarEstudios(Request $request){
        $trayectoriaAcademica = $request->trayectoriaAcademica;
        $empleado = Empleado::find($request->idEmpleado);
        if(!is_null($empleado)){
            $empleado->estudios()->delete();
            foreach ($trayectoriaAcademica as $estudioEmpleado){
                $estudio = new Estudio();
                $estudio->empleado_id = $empleado->id;
                $estudio->institucion = $estudioEmpleado['institucion'];
                $estudio->grado_de_estudio = $estudioEmpleado['grado_de_estudio'];
                $estudio->titulo = $estudioEmpleado['titulo'];
                $estudio->cedula = $estudioEmpleado['cedula'];
                $estudio->situacion_cedula = $estudioEmpleado['situacion_cedula'];
                $estudio->save();
            }
            return array('status'=>ResponseStatus::HTTP_OK,'mensaje'=>'Trajectoria academica editada correctamente');
        }else{
            return array('status'=>ResponseStatus::HTTP_NOT_FOUND,'mensaje'=>'No se localizo el empleado solicitado');
        }
    }

    public function estudiosEmpleado($idEmpleado){
        $empleado = Empleado::find($idEmpleado);
        if(is_null($empleado)){
            return Response::json(array('status'=>ResponseStatus::HTTP_NOT_FOUND,'data'=>array(),
                'mensaje'=>'No se ha localizado la información solicitada'));
        }
        return Response::json(array('status'=>ResponseStatus::HTTP_OK,'data'=>$empleado->estudios()->get()));
    }

    /****
     * @param Request $request
     * @return array
     */
    public function editEstudio(Request $request){
        $estudio = Estudio::find($request->id);
        if(!is_null($estudio)){
            $estudio->institucion = $request->institucion;
            $estudio->situacion_cedula = $request->situacion_cedula;
            $estudio->titulo = $request->titulo;
            $estudio->grado_de_estudio = $request->grado_de_estudio;
            $estudio->save();
            return array('status'=>ResponseStatus::HTTP_OK,'mensaje'=>'Trajectoria academica editada correctamente');
        }
        return Response::json(array('status'=>ResponseStatus::HTTP_NOT_FOUND,'data'=>array(),
            'mensaje'=>'No se ha localizado la información solicitada'));
    }

    /***
     * @param $id
     * @return mixed
     */
    public function estudio($id){
        $estudio = Estudio::find($id);
        if(is_null($estudio)){
            return Response::json(array('status'=>ResponseStatus::HTTP_NOT_FOUND,'data'=>array(),
                'mensaje'=>'No se ha localizado la información solicitada'));
        }
        return Response::json(array('status'=>ResponseStatus::HTTP_OK,'data'=>$estudio));
    }

    /***
     * @param $id
     * @return mixed
     */
    public function eliminarEstudio($id){
        $estudio = Estudio::find($id);
        if(is_null($estudio)){
            return Response::json(array('status'=>ResponseStatus::HTTP_NOT_FOUND,'data'=>array(),
                'mensaje'=>'No se ha localizado la información solicitada'));
        }
        $estudio->delete();
        return Response::json(array('status'=>ResponseStatus::HTTP_OK,'mensaje'=>'Estudio eliminado correctamente'));
    }

}
