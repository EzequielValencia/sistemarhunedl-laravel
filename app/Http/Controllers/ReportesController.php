<?php
/***
 * @author Ezequiel Valencia Moreno 7A turno mixto unedl
 *
 */
namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Excel;
use PeopleUnedl\Empleado;
use PeopleUnedl\Puesto;
use PeopleUnedl\Vicerrectoria;

class ReportesController extends Controller
{
    public function __construct(){
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }

    public function index(){
        return view('empleados.generadorReportes',["vicerrectorias"=>$this->vicerrectorias]);
    }

    private function crearReporteTodasLasVicerrectorias($fechaInicio,$fechaFin){
        $vicerectorias = Vicerrectoria::all();
        $empleadosPuestosVicerrectoria = [];
        foreach ($vicerectorias as $vicerectoria){
            $puestos = $vicerectoria->puestos()->get();
            array_push($empleadosPuestosVicerrectoria,['vicerrectoria' => $vicerectoria->nombre,
                'puestos'=>$puestos]);
            foreach ($puestos as $puesto){
                $this->sacaRelacionEmpleados($puesto,$fechaInicio,$fechaFin);
            }
        }

        return $empleadosPuestosVicerrectoria;
    }

    /***
     * Metodo para sacar los datos para el reporte.
     * Se saca la cantidad de empleados dados de baja durante el periodo solicitado,
     * la cantidad empleados que existen al inicio y al final del periodo,
     * la cantidad de empleados que se contrataron durante el periodo,
     * la cantidad de empleados que se dieron de baja durante el periodo
     * @param $puesto Puesto del cual se sacara las cantidades de los empleados
     * @param $fechaInicio Fecha en la que inicia el periodo a analizar
     * @param $fechaFin Fecha en la que termina el periodo a analizar
     */
    private function sacaRelacionEmpleados(&$puesto,$fechaInicio,$fechaFin){

        $empleadosExistentesAlInicio = Empleado::join('puestos_empleados','empleados.id','=','puestos_empleados.id_empleado')
            ->where('empleados.activo',true)
            ->whereNull('fecha_baja')
            ->whereNull('puestos_empleados.deleted_at')
            ->where('empleados.fecha_ingreso','<',date($fechaInicio))
            ->where('puestos_empleados.id_puesto',$puesto->id)->count();

        $empleadosExistentesAlFinal = Empleado::join('puestos_empleados','empleados.id','=','puestos_empleados.id_empleado')
            ->where('empleados.activo',true)
            ->whereNull('fecha_baja')
            ->whereNull('puestos_empleados.deleted_at')
            ->where('empleados.fecha_ingreso','<=',date($fechaFin))
            ->where('puestos_empleados.id_puesto',$puesto->id)->count();

        $empleadosContratadosDuranteElPeriodo = Empleado::join('puestos_empleados','empleados.id','=','puestos_empleados.id_empleado')
            ->whereNull('puestos_empleados.deleted_at')
            ->whereBetween('empleados.fecha_ingreso',[$fechaInicio,$fechaFin])
            ->where('puestos_empleados.id_puesto',$puesto->id)->count();

        $empleadosDadosDeBaja = Empleado::join('puestos_empleados','empleados.id','=','puestos_empleados.id_empleado')
            ->where('empleados.activo',false)
            ->whereBetween('fecha_baja',["$fechaInicio 00:00:00","$fechaFin 23:59:59"])
            ->whereNull('puestos_empleados.deleted_at')
            ->where('puestos_empleados.id_puesto',$puesto->id)->count();

        $puesto->cantidad_empleados_contratados_durante_el_periodo = $empleadosContratadosDuranteElPeriodo;
        $puesto->cantidad_empleados_existentes_al_final_del_periodo = $empleadosExistentesAlFinal;
        $puesto->cantidad_empleados_existentes_al_inicio_del_periodo = $empleadosExistentesAlInicio;
        $puesto->cantidad_empleados_dados_de_baja_durante_el_periodo = $empleadosDadosDeBaja;
        #$puesto->indice_de_rotacion = 0;
        $puesto->indice_de_rotacion = $this->caculaIndiceRotacion($empleadosContratadosDuranteElPeriodo,
            $empleadosDadosDeBaja,
            $empleadosExistentesAlInicio,
            $empleadosExistentesAlFinal);
    }

    private function caculaIndiceRotacion($empleadosContratadosDuranteElPeriodo,
                                          $empleadosDadosDeBaja,
                                          $empleadosExistentesAlInicio,
                                          $empleadosExistentesAlFinal){
        if(($empleadosExistentesAlInicio+$empleadosExistentesAlFinal) <= 0){

            return round((floatval(($empleadosContratadosDuranteElPeriodo + $empleadosDadosDeBaja)/ 2) * 100),2);
        }

        return round((floatval(($empleadosContratadosDuranteElPeriodo + $empleadosDadosDeBaja)/ 2) * 100)/floatval(($empleadosExistentesAlInicio+$empleadosExistentesAlFinal)/2),2,PHP_ROUND_HALF_UP);
    }

    private function creaReportePorVicerrectoria($idVicerrectoria,$fechaInicio,$fechaFin){
        $vicerrectoria = Vicerrectoria::find($idVicerrectoria);
        $puestos = $vicerrectoria->puestos()->get();
        $empleadosPuestosVicerrectoria = [];
        array_push($empleadosPuestosVicerrectoria,['vicerrectoria' => $vicerrectoria->nombre,
            'puestos'=>$puestos]);
        foreach ($puestos as $puesto){
            $this->sacaRelacionEmpleados($puesto,$fechaInicio,$fechaFin);
        }

        return $empleadosPuestosVicerrectoria;
    }

    private function creaReportePorPuesto($puesto,$fechaInicio,$fechaFin){

    }

    public function reporteEnPdf($fechaInicio,$fechaFin,$vicerrectoria,$puesto){
        $relacionPuestosCantidades = [];
        if($vicerrectoria=='todas'){
            $relacionPuestosCantidades = $this->crearReporteTodasLasVicerrectorias($fechaInicio,$fechaFin);
        }elseif ($vicerrectoria!='todas') {
            if($puesto == 'todos')
                $relacionPuestosCantidades = $this->creaReportePorVicerrectoria($vicerrectoria,$fechaInicio, $fechaFin);
            else
                $relacionPuestosCantidades = $this->creaReportePorPuesto($puesto,$fechaInicio, $fechaFin);
        }

        return \PDF::loadView('empleados.reporteEmpleados',["relacionPuestosCantidades"=>$relacionPuestosCantidades,
            'fechaInicio'=>$fechaInicio,"fechaFin"=>$fechaFin])->download("indice de rotación de personal $fechaInicio $fechaFin.pdf");
    }


}
