<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use PeopleUnedl\Logs;
use PeopleUnedl\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('usuarios.usuarios',['vicerrectorias'=>$this->vicerrectorias,'usuarios'=>User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            if(User::where('user_name',$request->user)->count()>0){
                throw new Exception("El nombre de usuario $request->user ya esta registrado",Response::HTTP_BAD_REQUEST);
            }
            $usuario = new User();
            $usuario->nombre = $request->nombre;
            $usuario->paterno = $request->paterno;
            $usuario->materno = is_null($request->materno)?'':$request->materno;
            $usuario->user_name = $request->user;
            $usuario->role = $request->role;
            $usuario->password = bcrypt($request->password);
            $usuario->activo = true;
            $usuario->creado_por = \Auth::id();
            $usuario->save();
            return ['status'=>Response::HTTP_OK,'mensaje'=>'usuario creado correctamente'];
        }catch(Exception $exception){

            return ['status'=>$exception->getCode(),'mensaje'=>$exception->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\Usuario  $usuario
     * @return mixed
     */
    public function show(Request $request)
    {
        try{
            $usuario = User::find($request->id);
            if(is_null($usuario))
                throw new Exception("Usuario $request->id no encontrado",Response::HTTP_NOT_FOUND);
            return array('status'=>200,'data'=>$usuario);
        }catch(Exception $e){
            $log = new Logs();
            $log->mensaje = $e->getMessage();
            $log->user_id = \Auth::id();
            $log->save();
            return array('staus'=>$e->getCode(),'mensaje'=>$e->getMessage(),'data'=>[]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\Usuario  $usuario
     * @return array
     */
    public function edit(Request $request)
    {
        $usuario = User::find($request->id);
        $usuario->nombre = $request->nombre;
        $usuario->paterno = $request->paterno;
        $usuario->materno = is_null($request->materno)?'':$request->materno;
        $usuario->user_name = $request->user;
        $usuario->role = $request->role;
        $usuario->activo = $request->activo == 'true'?true:false;
        $usuario->modificado_por = \Auth::id();
        $usuario->save();
        return array('status'=>Response::HTTP_OK,'mensaje'=>'usuario editado correctamente','data'=>[]);
    }

    public function cambiarPassword(Request $request){
        $usuario = User::find($request->id);
        $usuario->password = bcrypt($request->password);
        $usuario->modificado_por = \Auth::id();
        $usuario->save();
        return array('status'=>Response::HTTP_OK,'mensaje'=>'usuario editado correctamente','data'=>[]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $usuario = User::find($request->id);
        $usuario->delete();
        return array('status'=>Response::HTTP_OK,'mensaje'=>'usuario eliminado correctamente','data'=>[]);
    }

    public function historialUsuario($id){
        try{
            $usuario = User::find($id);
            return view('usuarios.historialPorUsuario',['usuario'=>$usuario,'vicerrectorias'=>$this->vicerrectorias]);
        }catch(Exception $e){

        }

    }
}
