<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use PeopleUnedl\Logs;
use PeopleUnedl\Vicerrectoria;
use Illuminate\Http\Request;


class VicerrectoriaController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('virectorias.vicerrectorias',['vicerrectorias'=>$this->vicerrectorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /***
     * Guarda la informacion de la vicerrectoria que se quiere dar de alta
     * @param Request $request con todos los datos a guardar
     * @return array informacion sobre el exito o el fallo al guarda los datos
     */
    public function store(Request $request)
    {
        try{
            $vicerectoroia = new Vicerrectoria();
            $vicerectoroia->nombre = $request->nombre;
            $vicerectoroia->codigo = $request->codigo;
            $vicerectoroia->descripcion = empty($request->descripcion) ? '':$request->descripcion;
            $vicerectoroia->creado_por = Auth::user()->id;
            $vicerectoroia->save();
            return ["status" => Response::HTTP_OK,'mensaje'=>"Se agrego correctamente la vicerrectoría"];
        }catch (Exception $e){
            $log = new Logs();
            $log->error = $e->getMessage();
            $log->user_id = Auth::user()->id;
            $log->save();

            return ["status" => Response::HTTP_INTERNAL_SERVER_ERROR];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\Vicerrectoria  $vicerrectoria
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rectoria = Vicerrectoria::find($request->id);
        return $rectoria;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\Vicerrectoria  $vicerrectoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Vicerrectoria $vicerrectoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\Vicerrectoria  $vicerrectoria
     * @return array
     */
    public function update(Request $request)
    {
        try{

            $vicerrectoria = Vicerrectoria::find($request->id);
            $vicerrectoria->nombre = $request->nombre;
            $vicerrectoria->codigo = $request->codigo;
            $vicerrectoria->descripcion = empty($request->descripcion) ? '':$request->descripcion;
            $vicerrectoria->save();

            return ["status" => Response::HTTP_OK,'mensaje'=>"Se modifico la vicerrectoría correctamente"];

        }catch(Exception $e){
            return ["status" => Response::HTTP_INTERNAL_SERVER_ERROR];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Vicerrectoria  $vicerrectoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{

            $vicerrectoria = Vicerrectoria::find($request->id);
            if(is_null($vicerrectoria)){

                throw new Exception("No existe la vicerrectoria $request->id");
            }else{
                $vicerrectoria->delete();
            }

            return ["status" => Response::HTTP_OK,'mensaje'=>"Se elimino la vicerrectoría correctamente"];

        }catch(Exception $e){
            $log = new Logs();
            $log->error = $e->getMessage();
            $log->user_id = Auth::user()->id;
            $log->save();
            return ["status" => Response::HTTP_INTERNAL_SERVER_ERROR,'mensaje'=>$e->getMessage()];
        }
    }
}
