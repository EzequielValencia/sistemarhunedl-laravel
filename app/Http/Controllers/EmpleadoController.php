<?php

namespace PeopleUnedl\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use PeopleUnedl\Empleado;
use Illuminate\Http\Request;
use PeopleUnedl\Logs;
use PeopleUnedl\Plantel;
use PeopleUnedl\Puesto;
use PeopleUnedl\PuestosEmpleado;
use PeopleUnedl\Vicerrectoria;

class EmpleadoController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }

    /**
     * Despliega la información de todos los empleados que esten activos
     * correspondiente a la vicerrectoria seleccionada
     * @param $rectoria Id de la vicerrectoria seleccionada
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($rectoria)
    {

        $empleados = Empleado::join('puestos_empleados','puestos_empleados.id_empleado','=','empleados.id')
            ->join('puestos','puestos.id','=','puestos_empleados.id_puesto')
            ->join('vicerrectorias','vicerrectorias.id','=','puestos.rectoria_id')
            ->whereNull('empleados.deleted_at')
            ->whereNull('empleados.fecha_baja')
            ->where('empleados.activo',true)
            ->whereNull('puestos.deleted_at')
            ->whereNull('puestos_empleados.deleted_at')
            ->where('vicerrectorias.codigo',$rectoria)->select('empleados.*')
            ->orderBy('created_at')->distinct()->get();
        $viceRectoriaSolicitada = Vicerrectoria::where('codigo',$rectoria)->first();
        return view('empleados.empleados',['vicerrectorias'=>$this->vicerrectorias,'empleados'=>$empleados,
            'editado'=>session('editado'),'guardado'=>session('guardadp'),'titulo'=>$viceRectoriaSolicitada->nombre]);
    }

    /***
     * Trae el registro de todos los empleados que esten activos en el sistema
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function todos()
    {

        $empleados = Empleado::whereNull('empleados.fecha_baja')->where('empleados.activo',true)->get();

        return view('empleados.empleados',['vicerrectorias'=>$this->vicerrectorias,'empleados'=>$empleados,
            'editado'=>session('editado'),'guardado'=>session('guardadp')]);
    }

    public function dadosDeBaja(){
        $empleados = Empleado::whereNotNull('empleados.fecha_baja')->where('empleados.activo',false)->get();
        return view('empleados.empleados',['vicerrectorias'=>$this->vicerrectorias,'empleados'=>$empleados,'titulo'=>'Dados de baja']);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vicerrectorias = Vicerrectoria::all();
        $puestosPorRectorias=[];
        foreach ($vicerrectorias as $vicerrectoria){

            $puestos=Puesto::where('rectoria_id',$vicerrectoria->id)->whereRaw('puestos.vacantes_usadas != puestos.vacantes')->get();
            array_push($puestosPorRectorias,['vicerrectoria'=>$vicerrectoria->nombre,'puestos'=>$puestos]);
        }
        $planteles = Plantel::all();
        return view('empleados.crearEmpleado',['vicerrectorias'=>$vicerrectorias,'puestosPorRectorias'=>$puestosPorRectorias
        ,'planteles'=>$planteles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $empleado = new Empleado();
            $empleado->nombre = $request->nombre;
            $empleado->apellido_paterno = $request->apellido_paterno;
            $empleado->apellido_materno = $request->apellido_materno;
            $empleado->fecha_nacimiento = $request->fecha_nacimiento;
            $empleado->curp = $request->curp;
            $empleado->rfc = $request->rfc;
            $empleado->numero_imss = $request->numero_imss;
            $empleado->numero_infonavit = $request->numero_infonavit;
            $empleado->lugar_de_nacimiento = $request->lugar_de_nacimiento;
            $empleado->cedula = $request->cedula;
            $empleado->titulo = $request->titulo;
            $empleado->estatus_cedula = $request->estatus_cedula;
            $empleado->cp = $request->cp;
            $empleado->domcicilio = $request->domcicilio;
            $empleado->fecha_matrimonio = $request->fecha_matrimonio;
            $empleado->telefono_de_emergencias = $request->telefono_de_emergencias;
            $empleado->telefono = $request->telefono;
            $empleado->celular = $request->celular;
            $empleado->correo = $request->correo;
            $empleado->codigo = $request->codigo;
            $empleado->fecha_ingreso = $request->fecha_ingreso;
            $empleado->fecha_alta_imss = $request->fecha_alta_imss;
            $empleado->salario = $request->salario;
            $empleado->estado_civil = $request->estado_civil;
            $empleado->creado_por = \Auth::user()->id;
            $empleado->sexo = $request->sexo;
            $empleado->patron = $request->patron;
            $empleado->plantel_id = $request->plantel;
            if($request->hasFile('imagen')) {
                $empleado->imagen = $request->file('imagen')->store('public');
            }
            $empleado->jefe = $request->jefe;
            $empleado->save();
            foreach ($request->cargo as $cargo){
                $puestoEmpleado = new PuestosEmpleado();
                $puestoEmpleado->id_puesto = $cargo;
                $puestoEmpleado->id_empleado = $empleado->id;
                $puestoEmpleado->save();
            }
            Puesto::whereIn('id',$request->cargo)->increment('vacantes_usadas');
            return Redirect::to('/empleados/todos')->with(["editado"=>true]);
        }catch(Exception $exception){
            $log = new Logs();
            $log->error = $exception->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $empleado = Empleado::find($request->id);
        $vicerrectorias = Vicerrectoria::all();
        $puestosPorRectorias=[];
        foreach ($vicerrectorias as $vicerrectoria){

            $puestos=Puesto::where('rectoria_id',$vicerrectoria->id)->get();
            array_push($puestosPorRectorias,['vicerrectoria'=>$vicerrectoria->nombre,'puestos'=>$puestos,'empleado'=>$empleado]);
        }
        $planteles = Plantel::all();
        return view('empleados.verEmpleado',['vicerrectorias'=>$vicerrectorias,'puestosPorRectorias'=>$puestosPorRectorias,'empleado'=>$empleado
            ,'planteles'=>$planteles]);
    }


    public  function getEmpleado(Request $request){
        $empleado = Empleado::find($request->id);

        $empleado->misPuestos = $empleado->misPuestos();
        return $empleado;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try{
            $empleado = Empleado::find($request->idEmpleado);
            $empleado->nombre = $request->nombre;
            $empleado->apellido_paterno = $request->apellido_paterno;
            $empleado->apellido_materno = $request->apellido_materno;
            $empleado->fecha_nacimiento = $request->fecha_nacimiento;
            $empleado->curp = $request->curp;
            $empleado->rfc = $request->rfc;
            $empleado->numero_imss = $request->numero_imss;
            $empleado->numero_infonavit = $request->numero_infonavit;
            $empleado->lugar_de_nacimiento = $request->lugar_de_nacimiento;
            $empleado->cedula = $request->cedula;
            $empleado->titulo = $request->titulo;
            $empleado->estatus_cedula = $request->estatus_cedula;
            $empleado->cp = $request->cp;
            $empleado->domcicilio = $request->domcicilio;
            $empleado->fecha_matrimonio = $request->fecha_matrimonio;
            $empleado->telefono_de_emergencias = $request->telefono_de_emergencias;
            $empleado->telefono = $request->telefono;
            $empleado->celular = $request->celular;
            $empleado->correo = $request->correo;
            $empleado->codigo = $request->codigo;
            $empleado->fecha_ingreso = $request->fecha_ingreso;
            $empleado->fecha_alta_imss = $request->fecha_alta_imss;
            $empleado->salario = $request->salario;
            $empleado->estado_civil = $request->estado_civil;
            $empleado->modificado_por = \Auth::user()->id;
            $empleado->sexo = $request->sexo;
            $empleado->patron = $request->patron;
            $empleado->plantel_id= $request->plantel;
            if($request->hasFile('imagen')) {
                Storage::delete($empleado->imagen);
                $empleado->imagen = $request->file('imagen')->store('public');
            }
            $empleado->jefe = $request->jefe;

            $empleado->save();
            Puesto::whereIn('id',$empleado->puestosEmpleado()->select('id_puesto')->get()->toArray())->decrement('vacantes_usadas');
            $empleado->puestosEmpleado()->delete();

            foreach ($request->cargo as $cargo){
                $puestoEmpleado = new PuestosEmpleado();
                $puestoEmpleado->id_puesto = $cargo;
                $puestoEmpleado->id_empleado = $empleado->id;
                $puestoEmpleado->save();
            }
            Puesto::whereIn('id',$request->cargo)->increment('vacantes_usadas');
            return Redirect::to('/empleados/todos')->with(["editado"=>true]);
        }catch(Exception $exception){
            $log = new Logs();
            $log->mensaje = $exception->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return back();
        }
    }

    /***
     * Metodo para dar de baja al empleado seleccionado. Esto no eliminara el registro de la base de datos,
     * simplemente lo pasara a un esto de inactividad.
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function darDeBaja(Request $request){
        try{

            $empleado = Empleado::find($request->id);
            $empleado->activo = false;
            $empleado->fecha_baja = date('Y-m-d H:i:s');
            Puesto::whereIn('id',$empleado->puestosEmpleado()->select('id_puesto')->get()->toArray())->decrement('vacantes_usadas');

            $empleado->save();
            return ['status'=>200,'mensaje'=>'Empleado dado de baja correctamente'];
        }catch(Exception $exception){
            $log = new Logs();
            $log->mensaje = $exception->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>500,'mensaje'=>'Error al dar de baja el registro'];
        }
    }

    /***
     * Metodo para dar de baja al empleado seleccionado. Esto no eliminara el registro de la base de datos,
     * simplemente lo pasara a un esto de inactividad.
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function restaurarEmpleado(Request $request){
        try{

            $empleado = Empleado::find($request->id);
            $empleado->activo = true;
            $empleado->fecha_baja = null;
            $empleado->save();
            return ['status'=>200,'mensaje'=>'Empleado restaurado correctamente'];
        }catch(Exception $exception){
            $log = new Logs();
            $log->mensaje = $exception->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>500,'mensaje'=>'Error al restaurar el registro'];
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        //
    }


    public function buscarPorCurpRfcOIMSS(Request $request){
        $empleado = Empleado::where("$request->campo",$request->busqueda)->first();
        return (is_null($empleado)?array():$empleado);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $empleado = Empleado::find($request->id);
        Storage::delete($empleado->imagen);
        $empleado->delete();

        return ['status'=>200,'mensaje'=>'Empleado eliminado con exito'];
    }

    /***
     * @param Request $request
     * @return array
     */
    public function getJefesDeLosPuestos(Request $request){
        $puestosJefes = Puesto::join('puestos_subordinados','puestos.id','=','puestos_subordinados.id_puesto_padre')
                        ->whereIn('puestos_subordinados.id_puesto_hijo',$request->puestosSeleccionados)
                        ->select('puestos.id')->distinct()->get();
        $empleadosEncontrados = [];

        foreach ($puestosJefes as $puestoJefe){
            $empleados = Empleado::join('puestos_empleados','empleados.id','=','puestos_empleados.id_empleado')
                        ->where('puestos_empleados.id_puesto',$puestoJefe->id)
                        ->where('empleados.activo',true)
                        ->whereNull('empleados.deleted_at')
                        ->whereNull('puestos_empleados.deleted_at')
                        ->select('empleados.id','empleados.nombre','empleados.apellido_paterno','empleados.apellido_materno')
                        ->distinct()->get();
            \Debugbar::info($empleados->count());
            foreach ($empleados as $empleado){
                array_push($empleadosEncontrados,$empleado);
            }
        }
        return $empleadosEncontrados;
    }

}
