<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Mockery\Exception;
use PeopleUnedl\Plantel;
use Illuminate\Http\Request;

class PlantelController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $planteles = Plantel::all();
        return view('planteles.planteles',["vicerrectorias"=>$this->vicerrectorias,"planteles"=>$planteles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            $plantel = new Plantel();
            $plantel->nombre = $request->nombre;
            $plantel->descripcion = $request->descripcion;
            $plantel->email = $request->email;
            $plantel->telefono = $request->telefono;
            $plantel->direccion = $request->direccion;
            $plantel->save();
            return array('mensaje'=>'Plantel creado creectamente','status'=>Response::HTTP_OK);
        }catch (Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $idPlantel
     * @return array
     */
    public function show($idPlantel = 0)
    {
        $plantel = Plantel::find($idPlantel);
        if(!is_null($plantel)){

            return array('plantel'=>$plantel,'status'=>Response::HTTP_OK);
        }else{
            return array('plantel'=>array(),'status'=>Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Request $request
     * @return array
     */
    public function edit(Request $request)
    {
        try{
            $plantel = Plantel::find($request->id);
            if(is_null($plantel)){
                throw new Exception('Plantel no localizado');
            }
            $plantel->nombre = $request->nombre;
            $plantel->descripcion = $request->descripcion;
            $plantel->email = $request->email;
            $plantel->telefono = $request->telefono;
            $plantel->direccion = $request->direccion;
            $plantel->save();
            return array('mensaje'=>'Plantel editado creectamente','status'=>Response::HTTP_OK);
        }catch(Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\Plantel  $plantel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plantel $plantel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Plantel  $plantel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plantel $plantel)
    {
        //
    }
}
