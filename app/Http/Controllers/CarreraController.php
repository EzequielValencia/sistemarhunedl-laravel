<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use PeopleUnedl\Carrera;
use Illuminate\Http\Request;
use PeopleUnedl\GradoEstudio;

class CarreraController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carreras = Carrera::all();
        $gradosDeEstudio = GradoEstudio::all();
        return view('carrerascursos.carrerascursos',["vicerrectorias"=>$this->vicerrectorias,'carreras'=>$carreras,'gradoDeEstudio'=>$gradosDeEstudio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            $carrera = new Carrera();
            $carrera->nombre = $request->nombre;
            $carrera->codigo = $request->codigo;
            $carrera->id_grado_estudio = $request->gradoEstudio;
            $carrera->creado_por  = \Auth::id();
            $carrera->save();
            return array('status'=>Response::HTTP_OK,'data'=>$carrera,'mensaje'=>'Registro guardado con exito');
        }catch(Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\Carrera  $carrera
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $carrera = Carrera::find($request->id);
        $carrera->gradoEstudio = $carrera->gradoDeEstudio->id;
        return $carrera;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function update(Request $request)
    {
        try{
            $carrera =  Carrera::find($request->id);
            $carrera->nombre = $request->nombre;
            $carrera->codigo = $request->codigo;
            $carrera->id_grado_estudio = $request->gradoEstudio;
            $carrera->modificado_por  = \Auth::id();
            $carrera->save();
            return array('status'=>Response::HTTP_OK,'data'=>$carrera,'mensaje'=>'Registro editado con exito');
        }catch(Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Carrera  $carrera
     * @return array
     */
    public function destroy(Request $request)
    {
        try{
            $carrera =  Carrera::find($request->id);
            $carrera->delete();
            return array('status'=>Response::HTTP_OK,'data'=>$carrera,'mensaje'=>'Registro eliminado con exito');
        }catch(Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }
}
