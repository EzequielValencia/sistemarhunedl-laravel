<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use PeopleUnedl\Materia;
use PeopleUnedl\Carrera;
use PeopleUnedl\GradoEstudio;
use Illuminate\Http\Request;

class MateriaController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carreras = Carrera::all();
        $gradosDeEstudio = GradoEstudio::all();
        $materias = Materia::all();
        return view('carrerascursos.materias',["vicerrectorias"=>$this->vicerrectorias,
            'carreras'=>$carreras,"materias"=>$materias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            $materia = new Materia();
            $materia->codigo = $request->codigo;
            $materia->nombre = $request->nombre;
            $materia->id_carrera = $request->carreraOCurso;
            $materia->creado_por = Auth::id();
            $materia->modificado_por = Auth::id();
            $materia->save();
            return array('status'=>Response::HTTP_OK,'data'=>$materia,'mensaje'=>'Registro guardado con exito');
        }catch (Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        return Materia::find($request->id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function edit(Materia $materia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $materia = Materia::find($request->id);
            $materia->codigo = $request->codigo;
            $materia->nombre = $request->nombre;
            $materia->id_carrera = $request->carreraOCurso;
            $materia->modificado_por = Auth::id();
            $materia->save();
            return array('status'=>Response::HTTP_OK,'data'=>$materia,'mensaje'=>'Registro editado con exito');
        }catch (Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $materia = Materia::find($request->id);
            $materia->delete();
            return array('status'=>Response::HTTP_OK,'data'=>$materia,'mensaje'=>'Registro eliminado con exito');
        }catch (Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$request->all(),'mensaje'=>$e->getMessage());
        }
    }

    public function materiasPorCurso($idCurso){
        return Carrera::find($idCurso)->materias()->get();
    }
}
