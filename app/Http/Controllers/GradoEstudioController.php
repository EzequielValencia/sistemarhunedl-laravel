<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Mockery\Exception;
use PeopleUnedl\GradoEstudio;
use Illuminate\Http\Request;

class GradoEstudioController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gradosDeEstudio = GradoEstudio::all();
        return view('ofertaAcademica.ofertaAcademica',['vicerrectorias'=>$this->vicerrectorias,'gradosDeEstidio'=>$gradosDeEstudio]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        try{
            $gradoEstudio = new GradoEstudio();
            $gradoEstudio->nombre = $request->nombre;
            $gradoEstudio->codigo = $request->codigo;
            $gradoEstudio->save();
            return array('status'=>Response::HTTP_OK,'data'=>array(),'mensaje'=>'Grado dado de alta con exito');
        }catch(Exception $e){
            return array('status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'data'=>$e,'mensaje'=>$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\GradoEstudio  $gradoEstudio
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $gradoEstudio = GradoEstudio::find($request->id);

        return $gradoEstudio;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\GradoEstudio  $gradoEstudio
     * @return \Illuminate\Http\Response
     */
    public function edit(GradoEstudio $gradoEstudio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function update(Request $request)
    {
        try{
            $gradoEstudio = GradoEstudio::find($request->id);
            if(is_null($gradoEstudio)){
                throw new Exception('Codigo no localizado',Response::HTTP_NOT_FOUND);
            }
            $gradoEstudio->nombre = $request->nombre;
            $gradoEstudio->codigo = $request->codigo;
            $gradoEstudio->save();
            return array('mensaje'=>'Registro editado con exito','data'=>array(),'status'=>Response::HTTP_OK);
        }catch(Exception $e){
            return array('mensaje'=>$e->getMessage(),'data'=>$e,'status'=>$e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\GradoEstudio  $gradoEstudio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $gradoEstudio = GradoEstudio::find($request->id);
            if(is_null($gradoEstudio)){
                throw new Exception('Codigo no localizado',Response::HTTP_NOT_FOUND);
            }
            $gradoEstudio->delete();
            return array('mensaje'=>'Registro eliminado con exito','data'=>array(),'status'=>Response::HTTP_OK);
        }catch(Exception $e){
            return array('mensaje'=>$e->getMessage(),'data'=>$e,'status'=>$e->getCode());
        }
    }
}
