<?php

namespace PeopleUnedl\Http\Controllers;

use PeopleUnedl\Carrera;
use PeopleUnedl\Horario;
use Illuminate\Http\Request;
use PeopleUnedl\Empleado;
use PeopleUnedl\Materia;
use Illuminate\Http\Response;

class HorarioController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($codigoEmpleado){
        $empleado = Empleado::where('codigo',$codigoEmpleado)->first();
        $carreras = Carrera::all();
        return view('empleados.cargaHorario',['empleado'=>$empleado,'vicerrectorias'=>$this->vicerrectorias,'carreras'=>$carreras]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        Horario::where('id_empleado',$request->id_empleado)->delete();
        $cargaHoraria = $request->carga_horaria;
        foreach ($cargaHoraria as $carga){
            $horario = new Horario();
            $horario->horas = $carga['horas'];
            $horario->id_empleado = $carga['id_empleado'];
            $horario->id_materia = $carga['id_materia'];
            $horario->save();
        }
        return array('status'=>Response::HTTP_OK,'data'=>array(),'mensaje'=>'Horario creado con exito');
    }

    /***
     * Retorna la carga horaria por el id del empleado que se pasa por el parametro
     * @param $idEmpleado
     * @return mixed
     */
    public function cargaHorariaPorEmpleado($idEmpleado){
        $horario = Horario::where('id_empleado',$idEmpleado)->select('horas','id_materia','id_empleado','id_materia')->get();
        foreach ($horario as $carga){
            $materia = Materia::find($carga->id_materia);
            if(!is_null($materia)){
                $carga->materia = $materia->nombre;
            }else{
                $carga->materia = null;
            }
            $carrera = Carrera::find($carga->id_curso);
            if(!is_null($carrera)){
                $carga->carrera = $carrera->nombre;
            }else{
                $carga->carrera = null;
            }

        }
        return $horario;
    }
}
