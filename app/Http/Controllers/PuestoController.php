<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Http\Response;
use Mockery\Exception;
use PeopleUnedl\Logs;
use PeopleUnedl\Puesto;
use Illuminate\Http\Request;
use PeopleUnedl\PuestosSubordinado;
use PeopleUnedl\Vicerrectoria;

class PuestoController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $puestos = Puesto::all();
        return view('puestos.puestos',['puestos'=>$puestos,'vicerrectorias'=>$this->vicerrectorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request){
        try{

            $puestosSubordinados = new PuestosSubordinado();
            $puesto = new Puesto();
            $puesto->codigo = $request->codigo;
            $puesto->nombre = $request->nombre;
            $puesto->rectoria_id = $request->vicerrectoria;
            $puesto->vacantes = $request->vacantes;
            $puesto->creado_por = \Auth::user()->id;
            $puesto->save();

            if($request->has('subordinados')){
                foreach ($request->subordinados as $subordinado){
                    $puestosSubordinados = new PuestosSubordinado();
                    $puestosSubordinados->id_puesto_padre = $puesto->id;
                    $puestosSubordinados->id_puesto_hijo = $subordinado;
                    $puestosSubordinados->save();
                }
            }
            return ['status'=>Response::HTTP_OK,'mensaje'=>'Puesto guardado correctamente'];
        }catch (Exception $exception){
            $log = new Logs();
            $log->mensaje = $exception->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'mensaje'=>'No se pudo guardar el puesto'];
        }

    }

    /**
     * Retorna el puesto especificado por el usuario.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function show(Request $request)
    {
        try{
            $puesto = Puesto::find($request->id);
            if(is_null($puesto)){
                throw new Exception("No existe el puesto solicitado");
            }
            $puesto->subordinados = $puesto->misSubordinados();
            $puesto->vicerrectoria = $puesto->vicerrectoria()->first();
            return $puesto;
        }catch (Exception $e){
            $log = new Logs();
            $log->mensaje = $e->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>Response::HTTP_NOT_FOUND,'mensaje'=>'No se localizo el puesto solicitado'];
        }
    }




    public function puestosPorRectorias(Request $request){
        return Puesto::where('rectoria_id',$request->rectoriaId)->get();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function edit(Request $request){

        try{

            $puesto = Puesto::find($request->id);
            $puesto->nombre = $request->nombre;
            $puesto->codigo = $request->codigo;
            $puesto->rectoria_id = $request->vicerrectoria;
            $puesto->vacantes = $request->vacantes;
            $puesto->save();

            if($request->has('subordinados')){
                $puesto->subordinados()->delete();
                foreach ($request->subordinados as $subordinado){
                    $puestosSubordinados = new PuestosSubordinado();
                    $puestosSubordinados->id_puesto_padre = $puesto->id;
                    $puestosSubordinados->id_puesto_hijo = $subordinado;
                    $puestosSubordinados->save();
                }
            }
            return ['status'=>Response::HTTP_OK,'mensaje'=>'Puesto guardado correctamente'];
        }catch(Exception $e){
            $log = new Logs();
            $log->mensaje = $e->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>Response::HTTP_NOT_FOUND,'mensaje'=>'No se localizo el puesto solicitado'];
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\Puesto  $puesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Puesto $puesto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function destroy(Request $reques){
        try{
            $puesto = Puesto::find($reques->id);
            if(is_null($puesto)){
                throw new Exception("El puesto solicitado '$reques->id' no existe");
            }else{
                $puesto->delete();
                return ['status'=>Response::HTTP_OK,'mensaje'=>'Se elimino el puesto correctamente'];
            }
        }catch(Exception $e){
            $log = new Logs();
            $log->mensaje = $e->getMessage();
            $log->user_id = \Auth::user()->id;
            $log->save();
            return ['status'=>Response::HTTP_INTERNAL_SERVER_ERROR,'mensaje'=>'No se pudo eliminar el puesto'];
        }
    }
}
