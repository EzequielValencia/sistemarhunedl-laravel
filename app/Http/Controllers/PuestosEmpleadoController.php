<?php

namespace PeopleUnedl\Http\Controllers;

use PeopleUnedl\PuestosEmpleado;
use Illuminate\Http\Request;

class PuestosEmpleadoController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('unedlMiddleware');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \PeopleUnedl\PuestosEmpleado  $puestosEmpleado
     * @return \Illuminate\Http\Response
     */
    public function show(PuestosEmpleado $puestosEmpleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PeopleUnedl\PuestosEmpleado  $puestosEmpleado
     * @return \Illuminate\Http\Response
     */
    public function edit(PuestosEmpleado $puestosEmpleado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PeopleUnedl\PuestosEmpleado  $puestosEmpleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PuestosEmpleado $puestosEmpleado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PeopleUnedl\PuestosEmpleado  $puestosEmpleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(PuestosEmpleado $puestosEmpleado)
    {
        //
    }
}
