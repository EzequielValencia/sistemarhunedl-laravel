<?php

namespace PeopleUnedl\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use PeopleUnedl\Vicerrectoria;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $vicerrectorias;
    public function __construct(){
        $this->vicerrectorias = Vicerrectoria::all();
    }
}
