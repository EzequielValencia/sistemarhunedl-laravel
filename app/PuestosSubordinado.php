<?php

namespace PeopleUnedl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PuestosSubordinado extends Model
{
    use SoftDeletes;
    //

    public function puestoJefe(){
        return $this->hasMany('PeopleUnedl\Puesto','id','id_puesto_padre');
    }

    public function puestosSubordinados(){
        return $this->hasMany('PeopleUnedl\Puesto','id','id_puesto_hijo');
    }
}
