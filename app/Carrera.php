<?php

namespace PeopleUnedl;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrera extends Model
{
    use SoftDeletes,CascadeSoftDeletes;
    protected $cascadeDeletes = ['materias'];
    //
    public function gradoDeEstudio(){
        return $this->hasOne('PeopleUnedl\GradoEstudio','id','id_grado_estudio');
    }

    public function materias(){
        return $this->hasMany('PeopleUnedl\Materia','id_carrera','id');
    }
}
