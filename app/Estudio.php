<?php

namespace PeopleUnedl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudio extends Model
{
    use SoftDeletes;

    public function empleado(){
        return $this->hasOne(Empleado::class,'id','empleado_id');
    }
}
