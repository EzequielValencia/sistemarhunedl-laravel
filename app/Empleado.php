<?php

namespace PeopleUnedl;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleado extends Model
{
    use SoftDeletes,CascadeSoftDeletes;
    protected $cascadeDeletes = ['puestosEmpleado','cargaHoraria','estudios'];
    /***
     * Regresa la relacion de uno a muchos de empleado con sus puestos.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function puestosEmpleado(){
        return $this->hasMany(PuestosEmpleado::class,'id_empleado','id');
    }

    public function misPuestos(){
        $puestosEmpleado = $this->puestosEmpleado()->get();
        $puestos = [];
        foreach ($puestosEmpleado as $miPuesto){
               $puesto = Puesto::find($miPuesto->id_puesto);
               array_push($puestos,$puesto);
        }
        return $puestos;
    }

    /***
     * @author Ezequiel Valencia Moreno
     * Verifica si el id del puesto que esta entrando esta asignado al empleado.
     * Si no lo esta devuelve un false
     * @param $id id del puesto a verificar
     * @return bool
     */
    public function tengoAsignadoEstePuesto($id){
        $puestosEmpleado = $this->puestosEmpleado()->get();
        foreach ($puestosEmpleado as $miPuesto){
            if($miPuesto->id_puesto == $id){
                return true;
            }
        }
        return false;
    }

    public function cargaHoraria(){
        return $this->hasMany(Horario::class,'id_empleado','id');
    }

    public function estudios(){
        return $this->hasMany(Estudio::class);
    }
}
