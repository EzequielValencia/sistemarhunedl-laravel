<?php

namespace PeopleUnedl;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vicerrectoria extends Model
{
    use SoftDeletes,CascadeSoftDeletes;
    //
    protected $cascadeDeletes = ['puestos'];
    protected $dates = ['deleted_at'];

    public function puestos(){
        return $this->hasMany('PeopleUnedl\Puesto','rectoria_id','id');
    }
}
