<?php

namespace PeopleUnedl;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Puesto extends Model
{
    use SoftDeletes,CascadeSoftDeletes;
    protected $cascadeDeletes = ['subordinados'];
    //
    public function vicerrectoria(){
        return $this->hasOne('PeopleUnedl\Vicerrectoria','id','rectoria_id');
    }

    /**
     * Regresa un arreglo con la relacion ['id_puesto_padre'=>,'id_puesto_hijo'=>]
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subordinados(){
        return $this->hasMany('PeopleUnedl\PuestosSubordinado','id_puesto_padre','id');
    }

    /**
     * Retorna una arreglo con la relacion ['id_puesto_padre'=>,'id_puesto_hijo'=>]
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jefes(){
        return $this->hasMany('PeopleUnedl\PuestosSubordinado','id_puesto_hijo','id');
    }

    /**
     * Regresa un arreglo con los objetos de los puestos subordinados.
     *
     * @return array
     */
    public function misSubordinados(){
        $subordinadosIds = $this->subordinados()->get();
        $subordinados=[];
        foreach ($subordinadosIds as $subordinado){
            array_push($subordinados, $this->find($subordinado->id_puesto_hijo) );
        }
        return $subordinados;
    }

    public function misJefes(){
        $idsMisJefes = $this->jefes()->get();
        $jefes=[];
        foreach ($idsMisJefes as $jefe){
            array_push($jefes, $this->find($jefe->id_puesto_padre ));
        }
        return $jefes;
    }

    public function empleadosPuesto(){
        return $this->hasMany('PeopleUnedl\PuestosEmpleado','id_puesto','id');
    }
}
