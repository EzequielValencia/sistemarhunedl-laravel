<?php

namespace PeopleUnedl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materia extends Model
{
    use SoftDeletes;
    //

    public function carreraCurso(){
        return  $this->hasOne('PeopleUnedl\Carrera','id','id_carrera');
    }
}
