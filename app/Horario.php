<?php

namespace PeopleUnedl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Horario extends Model
{
    use SoftDeletes;
    /***
     * Retorna la carrera o curso al que esta relacionado el horario
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function curso(){
        return $this->hasOne(Carrera::class,'id','id_curso');
    }

    public function materia(){
        return $this->hasOne(Materia::class,'id','id_materia');
    }
}
